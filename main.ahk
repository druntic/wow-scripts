#SingleInstance Force
#Include, setting\help functions.ahk
Gui 1:Font, s10 Bold  cBlack
Gui, 1:Add, Picture, x-36 y-31 w648 h590 , setting\icc.png
Gui, 1:Add, Text, x210 y9 w200 h35 BackgroundTrans , Wotlk scripts by Dado
Gui, 1:Add, Picture, x52 y59 w60 h60 vret gret, pala\divine.ico
Gui, 1:Add, Picture, x142 y59 w60 h60 vfury gfury , warrior\whirlwind.ico
Gui, 1:Add, Picture, x232 y59 w60 h60 vferal gferal, feral\shred.ico
Gui, 1:Add, Picture, x322 y59 w60 h60 vboomy gboomy, moonkin\moonkin.ico
Gui, 1:Add, Picture, x412 y59 w60 h60 vmage gmage, mage\mage.ico
Gui, 1:Add, Picture, x52 y149 w60 h60 vudk gudk, dk pics\scourge.ico
Gui, 1:Add, Picture, x142 y149 w60 h60 vfdk gfdk, dk pics\frost.ico
Gui, 1:Add, Picture, x232 y149 w60 h60 vbdk gbdk, dk pics\blood.ico
Gui, 1:Add, Picture, x322 y149 w60 h60 vhunter ghunter, hunter\hunter.ico
Gui, 1:Add, Picture, x412 y149 w60 h60 vdemo gdemo, warlock\demo.ico
Gui, 1:Add, Picture, x52 y239 w60 h60 vrogue grogue, rogue\rogue.ico
Gui, 1:Add, Picture, x142 y239 w60 h60 vpriest gpriest, priest\priest.ico
Gui, 1:Add, Picture, x412 y429 w60 h60 vsettings gsettings , setting\settings.png
Gui, 1:Add, Text, x67 y124 w40 h20 BackgroundTrans, Ret
Gui, 1:Add, Text, x157 y124 w40 h20 BackgroundTrans, Fury
Gui, 1:Add, Text, x242 y124 w40 h20 BackgroundTrans, Feral
Gui, 1:Add, Text, x323 y124 w70 h20 BackgroundTrans, Boomkin
Gui, 1:Add, Text, x423 y124 w40 h20 BackgroundTrans, Mage
Gui, 1:Add, Text, x50 y214 w80 h20 BackgroundTrans, Unholy dk
Gui, 1:Add, Text, x144 y214 w80 h20 BackgroundTrans, Frost dk
Gui, 1:Add, Text, x230 y214 w80 h20 BackgroundTrans, Blood dk
Gui, 1:Add, Text, x328 y214 w80 h20 BackgroundTrans, Hunter
Gui, 1:Add, Text, x405 y214 w80 h20 BackgroundTrans, Demo lock
Gui, 1:Add, Text, x56 y300 w80 h20 BackgroundTrans, Rogue
Gui, 1:Add, Text, x153 y300 w80 h20 BackgroundTrans, Priest
Gui, 1:Add, Picture, x112 y59 w15 h15 vretbinds gretbinds, setting\help.png
Gui, 1:Add, Picture, x202 y59 w15 h15 vwarrbinds gwarrbinds, setting\help.png
Gui, 1:Add, Picture, x292 y59 w15 h15 vferalbinds gferalbinds, setting\help.png
Gui, 1:Add, Picture, x382 y59 w15 h15 vboomkinbinds gboomkinbinds, setting\help.png
Gui, 1:Add, Picture, x472 y59 w15 h15 vmagebinds gmagebinds, setting\help.png
Gui, 1:Add, Picture, x112 y149 w15 h15 vunholybinds gunholybinds, setting\help.png
Gui, 1:Add, Picture, x202 y149 w15 h15 vfrostbinds gfrostbinds, setting\help.png
Gui, 1:Add, Picture, x292 y149 w15 h15 vbloodbinds gbloodbinds, setting\help.png
Gui, 1:Add, Picture, x382 y149 w15 h15 vhunterbinds ghunterbinds, setting\help.png
Gui, 1:Add, Picture, x472 y149 w15 h15 vdemobinds gdemobinds, setting\help.png
Gui, 1:Add, Picture, x112 y239 w15 h15 vroguebinds groguebinds, setting\help.png
Gui, 1:Add, Picture, x202 y239 w15 h15 vpriestbinds gpriestbinds, setting\help.png
Gui, 1:Add, Picture, x527 y0 w15 h15 vinfo ginfo, setting\info.png
Gui, 1:Add, Picture, x472 y74 w15 h15 vmagecalibration gmagecalibration, setting\settings.png
Gui, 1:Add, Picture, x112 y74 w15 h15 vretcalibration gretcalibration, setting\settings.png
Gui, 1:Add, Picture, x202 y74 w15 h15 vwarriorcalibration gwarriorcalibration, setting\settings.png
Gui, 1:Add, Picture, x292 y74 w15 h15 vferalcalibration gferalcalibration, setting\settings.png
Gui, 1:Add, Picture, x112 y164 w15 h15 vunholycalibration gunholycalibration, setting\settings.png
Gui, 1:Add, Picture, x292 y164 w15 h15 vbloodcalibration gbloodcalibration, setting\settings.png
Gui, 1:Add, Picture, x202 y164 w15 h15 vfrostcalibration gfrostcalibration, setting\settings.png
Gui, 1:Add, Picture, x472 y164 w15 h15 vdemowarlockcalibration gdemowarlockcalibration, setting\settings.png
Gui, 1:Add, Picture, x202 y254 w15 h15 vpriestcalibration gpriestcalibration, setting\settings.png
Gui, 1:Add, Picture, x112 y254 w15 h15 vroguecalibration groguecalibration, setting\settings.png
Gui, 1:Add, Picture, x382 y164 w15 h15 vhuntercalibration ghuntercalibration, setting\settings.png
Gui, 1:Add, Picture, x382 y74 w15 h15 vboomkincalibration gboomkincalibration, setting\settings.png

; Generated using SmartGUI Creator 4.0
ret= , fury= , feral= ,boomy= ,mage= ,udk= ,fdk= ,demo= ,hunter= , bdk=, priest= 
retbinds_TT=Set Ret binds
warrbinds_TT=Set Fury Warr binds
feralbinds_TT=Set Kitty binds
boomkinbinds_TT=Set Boomkin binds
magebinds_TT=Set Mage Fire binds
unholybinds_TT=Set Unholy DK binds
frostbinds_TT=Set Frost DK binds
bloodbinds_TT=Set Blood DK binds
hunterbinds_TT=Set Hunter binds
roguebinds_TT=Set Rogue binds
demobinds_TT=Set Demo Lock binds
priestbinds_TT=Set Priest binds
settings_TT=Use this for classes which use a rotation addon like CLCRet or CLCDk. Drag the red square so that the rotation addon is inside of it. Press F0 to exit and F1 to save coordinates
info_TT=press Alt to pause/resume the script. 1 beep means its on and 2 beeps means its off.
magecalibration_TT = Click to calibrate mage boxes
retcalibration_TT = Click to calibrate retribution boxes
warriorcalibration_TT = Click to calibrate warrior boxes
feralcalibration_TT = Click to calibrate feral boxes
frostcalibration_TT = Click to calibrate frost boxes
unholycalibration_TT = Click to calibrate unholy boxes
bloodcalibration_TT = Click to calibrate blood boxes
demowarlockcalibration_TT = Click to calibrate demo warlock boxes
priestcalibration_TT = Click to calibrate priest boxes
roguecalibration_TT = Click to calibrate rogue boxes
huntercalibration_TT = Click to calibrate hunter boxes
boomkincalibration_TT = Click to calibrate moonkin boxes
Gui, 1:Show, x731 y357 h531 w541, Wotlk scripts

OnMessage(0x200, "WM_MOUSEMOVE")
Return

GuiClose:
    ExitApp
info:
    return 
ret:
    Run, pala\paladin script.ahk
    ExitApp
    return
fury:
    Run, warrior\warrior script.ahk
    ExitApp
    return
feral:
    Run, feral\feral script.ahk
    ExitApp
    return
boomy:
    Run, moonkin\balance wo starfall script.ahk
    ExitApp
    return
mage:
    Run, mage\mage script.ahk
    ExitApp
    return
udk:
    Run, dk pics\unholy dk script.ahk
    ExitApp
    return
fdk:
    Run, dk pics\frost dk script.ahk
    ExitApp
    return
bdk:
    Run, dk pics\blood dk script.ahk
    ExitApp
    return
hunter:
    Run, hunter\hunter script.ahk
    ExitApp
    return
demo:
    Run, warlock\demo warlock script.ahk
    ExitApp
    return
rogue:
    Run, rogue\rogue script.ahk
    ExitApp
    return
priest:
    Run, priest\priest script.ahk
    ExitApp
    return
settings:
    Run, setting\calibrate.ahk
    ExitApp
    return
magecalibration:
    Run, mage\mageCalibrate.ahk
    ExitApp
    return
retcalibration:
    Run, pala\retCalibrate.ahk
    ExitApp
    return
warriorcalibration:
    Run, warrior\warriorCalibrate.ahk
    ExitApp
    return
feralcalibration:
    Run, feral\feralCalibrate.ahk
    ExitApp
    return
frostcalibration:
    Run, dk pics\fdkCalibrate.ahk
    ExitApp
    return
unholycalibration:
    Run, dk pics\udkCalibrate.ahk
    ExitApp
    return
bloodcalibration:
    Run, dk pics\bdkCalibrate.ahk
    ExitApp
    return
priestcalibration:
    Run, priest\priestCalibrate.ahk
    ExitApp
    return
demowarlockcalibration:
    Run, warlock\demoWarlockCalibrate.ahk
    ExitApp
    return
roguecalibration:
    Run, rogue\rogueCalibrate.ahk
    ExitApp
    return
huntercalibration:
    Run, hunter\hunterCalibrate.ahk
    ExitApp
    return
boomkincalibration:
    Run, moonkin\moonkinCalibrate.ahk
    ExitApp
    return
retbinds:
    
    Gui, 2:Destroy
    Gui, 2:Add, Text, x62 y9 w110 h30 , Set Ret binds
    ;get current binds
    FileReadLine, palabinds, pala\binds.txt, 1
    ;StringRight, Divinestorm, palabinds, 1
    Divinestorm:=linewithjustskill(palabinds)
    FileReadLine, palabinds, pala\binds.txt, 2
    Crusader:=linewithjustskill(palabinds)
    FileReadLine, palabinds, pala\binds.txt, 3
    Judgement:=linewithjustskill(palabinds)
    FileReadLine, palabinds, pala\binds.txt, 4
    Exorcism:=linewithjustskill(palabinds)
    FileReadLine, palabinds, pala\binds.txt, 5
    Hammer:=linewithjustskill(palabinds)
    FileReadLine, palabinds, pala\binds.txt, 6
    Cons:=linewithjustskill(palabinds)
    FileReadLine, palabinds, pala\binds.txt, 7
    Shield:=linewithjustskill(palabinds)
    FileReadLine, palabinds, pala\binds.txt, 8
    Seals2:=linewithjustskill(palabinds)
    Gui, 2:Add, Hotkey, x102 y49 w80 h30  vDivinestorm2 , %Divinestorm%
    Gui, 2:Add, Hotkey, x102 y89 w80 h30 vCrusader2, %Crusader%
    Gui, 2:Add, Hotkey, x102 y129 w80 h30 vJudgement2, %Judgement%
    Gui, 2:Add, Hotkey, x102 y169 w80 h30 vExorcism2, %Exorcism%
    Gui, 2:Add, Hotkey, x102 y209 w80 h30 vHammer2, %Hammer%
    Gui, 2:Add, Hotkey, x102 y249 w80 h30 vCons2, %Cons%
    Gui, 2:Add, Hotkey, x102 y289 w80 h30 vShield2, %Shield%
    Gui, 2:Add, Hotkey, x102 y329 w80 h30 vSeal2, %Seals2%
    Gui, 2:Add, Text, x22 y54 w70 h30 , Divine Storm
    Gui, 2:Add, Text, x-8 y664 w130 h-560 , Text
    Gui, 2:Add, Text, x22 y94 w70 h30 , Crusader Strike
    Gui, 2:Add, Text, x22 y134 w70 h30 , Judgement
    Gui, 2:Add, Text, x22 y174 w70 h30 , Exorcism
    Gui, 2:Add, Text, x22 y214 w70 h30 , Hammer of Wrath
    Gui, 2:Add, Text, x22 y254 w70 h30 , Consecration
    Gui, 2:Add, Text, x22 y294 w70 h30 , Sacred Shield
    Gui, 2:Add, Text, x22 y334 w70 h30 , Seals
    Gui, 2:Add, Button, x102 y369 w80 h30 gcancel , Cancel
    Gui, 2:Add, Button, x22 y369 w70 h30  gapply, Apply
    ; Generated using SmartGUI Creator 4.0
    Gui, 2:Show, x774 y366 h408 w211, Ret binds
    return

    cancel:
        ExitApp
        return
    apply:
        Gui, Submit
        l:=StrLen(Divinestorm2)
        if l>0:
            replace(Divinestorm2,1,"pala\binds.txt")
        l:=StrLen(Crusader2)
        if l>0:
            replace(Crusader2,2,"pala\binds.txt")
        l:=StrLen(Judgement2)
        if l>0:
            replace(Judgement2,3,"pala\binds.txt")
        l:=StrLen(Exorcism2)
        if l>0:
            replace(Exorcism2,4,"pala\binds.txt")
        l:=StrLen(Hammer2)
        if l>0:
            replace(Hammer2,5,"pala\binds.txt")
        l:=StrLen(Cons2)
        if l>0:
            replace(Cons2,6,"pala\binds.txt")
        l:=StrLen(Shield2)
        if l>0:
            replace(Shield2,7,"pala\binds.txt")
        return
warrbinds:
    Gui, 2:Destroy
    Gui, 2:Add, Text, x62 y9 w110 h30 , Set kitty binds
    ;get current binds
    FileReadLine, warrbinds, warrior\binds.txt, 1
    Bloodthirst:=linewithjustskill(warrbinds)
    FileReadLine, warrbinds, warrior\binds.txt, 2
    Execute:=linewithjustskill(warrbinds)
    FileReadLine, warrbinds, warrior\binds.txt, 3
    Heroic:=linewithjustskill(warrbinds)
    FileReadLine, warrbinds, warrior\binds.txt, 4
    Slam:=linewithjustskill(warrbinds)
    FileReadLine, warrbinds, warrior\binds.txt, 5
    Sunder:=linewithjustskill(warrbinds)
    FileReadLine, warrbinds, warrior\binds.txt, 6
    whirlwind:=linewithjustskill(warrbinds)

    Gui, 2:Add, Hotkey, x102 y49 w80 h30  vBloodthirst2 , %Bloodthirst%
    Gui, 2:Add, Hotkey, x102 y89 w80 h30 vExecute2, %Execute%
    Gui, 2:Add, Hotkey, x102 y129 w80 h30 vHeroic2, %Heroic%
    Gui, 2:Add, Hotkey, x102 y169 w80 h30 vSlam2, %Slam%
    Gui, 2:Add, Hotkey, x102 y209 w80 h30 vSunder2, %Sunder%
    Gui, 2:Add, Hotkey, x102 y249 w80 h30 vWhirlwind2, %Whirlwind%
    Gui, 2:Add, Text, x22 y54 w70 h30 , Bloodthirst
    Gui, 2:Add, Text, x-8 y659 w130 h-560 , Text
    Gui, 2:Add, Text, x22 y95 w70 h30 , Execute
    Gui, 2:Add, Text, x22 y134 w70 h30 , Heroic Throw
    Gui, 2:Add, Text, x22 y174 w70 h30 , Slam
    Gui, 2:Add, Text, x22 y214 w70 h30 , Sunder Armor
    Gui, 2:Add, Text, x22 y254 w70 h30 , Whirlwind
    Gui, 2:Add, Button, x102 y349 w80 h30 gcancel2 , Cancel
    Gui, 2:Add, Button, x22 y349 w70 h30  gapply2, Apply
    ; Generated using SmartGUI Creator 4.0
    Gui, 2:Show, x774 y366 h408 w211, Warr binds
    return

    cancel2:
        ExitApp
        return
    apply2:
        Gui, Submit
        l:=StrLen(Bloodthirst2)
        if l>0:
            replace(Bloodthirst2,1,"warrior\binds.txt")
        l:=StrLen(Execute2)
        if l>0:
            replace(Execute2,2,"warrior\binds.txt")
        l:=StrLen(Heroic2)
        if l>0:
            replace(Heroic2,3,"warrior\binds.txt")
        l:=StrLen(Slam2)
        if l>0:
            replace(Slam2,4,"warrior\binds.txt")
        l:=StrLen(Sunder2)
        if l>0:
            replace(Sunder2,5,"warrior\binds.txt")
        l:=StrLen(Whirlwind2)
        if l>0:
            replace(Whirlwind2,6,"warrior\binds.txt")
        return
feralbinds:
    Gui, 2:Destroy
    Gui, 2:Add, Text, x62 y9 w110 h30 , Set kitty binds
    ;get current binds
    FileReadLine, feralbinds, feral\binds.txt, 1
    Mangle:=linewithjustskill(feralbinds)
    FileReadLine, feralbinds, feral\binds.txt, 2
    Rake:=linewithjustskill(feralbinds)
    FileReadLine, feralbinds, feral\binds.txt, 3
    Rip:=linewithjustskill(feralbinds)
    FileReadLine, feralbinds, feral\binds.txt, 4
    Shred:=linewithjustskill(feralbinds)
    FileReadLine, feralbinds, feral\binds.txt, 5
    Fury:=linewithjustskill(feralbinds)
    FileReadLine, feralbinds, feral\binds.txt, 6
    Bite:=linewithjustskill(feralbinds)
    FileReadLine, feralbinds, feral\binds.txt, 7
    Faerie:=linewithjustskill(feralbinds)
    FileReadLine, feralbinds, feral\binds.txt, 8
    Savage:=linewithjustskill(feralbinds)
    FileReadLine, feralbinds, feral\binds.txt, 9
    Swipe:=linewithjustskill(feralbinds)
    FileReadLine, feralbinds, feral\binds.txt, 10
    Berserk:=linewithjustskill(feralbinds)

    Gui, 2:Add, Hotkey, x102 y49 w80 h30  vMangle2 , %Mangle%
    Gui, 2:Add, Hotkey, x102 y89 w80 h30 vRake2, %Rake%
    Gui, 2:Add, Hotkey, x102 y129 w80 h30 vRip2, %Rip%
    Gui, 2:Add, Hotkey, x102 y169 w80 h30 vShred2, %Shred%
    Gui, 2:Add, Hotkey, x102 y209 w80 h30 vFury2, %Fury%
    Gui, 2:Add, Hotkey, x102 y249 w80 h30 vBite2, %Bite%
    Gui, 2:Add, Hotkey, x102 y289 w80 h30 vFaerie2, %Faerie%
    Gui, 2:Add, Hotkey, x102 y329 w80 h30 vSavage2, %Savage%
    Gui, 2:Add, Hotkey, x102 y369 w80 h30 vSwipe2, %Swipe%
    Gui, 2:Add, Hotkey, x102 y409 w80 h30 vBerserk2, %Berserk%
    Gui, 2:Add, Text, x22 y54 w70 h30 , Mangle
    Gui, 2:Add, Text, x-8 y659 w130 h-560 , Text
    Gui, 2:Add, Text, x22 y95 w70 h30 , Rake
    Gui, 2:Add, Text, x22 y134 w70 h30 , Rip
    Gui, 2:Add, Text, x22 y174 w70 h30 , Shred
    Gui, 2:Add, Text, x22 y214 w70 h30 , Fury
    Gui, 2:Add, Text, x22 y254 w70 h30 , Bite
    Gui, 2:Add, Text, x22 y294 w70 h30 , Faerie
    Gui, 2:Add, Text, x22 y334 w70 h30 , Savage
    Gui, 2:Add, Text, x22 y374 w70 h30 , Swipe
    Gui, 2:Add, Text, x22 y414 w70 h30 , Berserk
    Gui, 2:Add, Button, x102 y449 w80 h30 gcancel3 , Cancel
    Gui, 2:Add, Button, x22 y449 w70 h30  gapply3, Apply
    ; Generated using SmartGUI Creator 4.0
    Gui, 2:Show, x774 y366 h488 w211, Kitty binds
    return

    cancel3:
        ExitApp
        return
    apply3:
        Gui, Submit
        l:=StrLen(Mangle2)
        if l>0:
            replace(Mangle2,1,"feral\binds.txt")
        l:=StrLen(Rake2)
        if l>0:
            replace(Rake2,2,"feral\binds.txt")
        l:=StrLen(Rip2)
        if l>0:
            replace(Rip2,3,"feral\binds.txt")
        l:=StrLen(Shred2)
        if l>0:
            replace(Shred2,4,"feral\binds.txt")
        l:=StrLen(Fury2)
        if l>0:
            replace(Fury2,5,"feral\binds.txt")
        l:=StrLen(Bite2)
        if l>0:
            replace(Bite2,6,"feral\binds.txt")
        l:=StrLen(Faerie2)
        if l>0:
            replace(Faerie2,7,"feral\binds.txt")
        l:=StrLen(Savage2)
        if l>0:
            replace(Savage2,8,"feral\binds.txt")
        l:=StrLen(Swipe2)
        if l>0:
            replace(Swipe2,9,"feral\binds.txt")
        l:=StrLen(Berserk2)
        if l>0:
            replace(Berserk2,10,"feral\binds.txt")
        return
boomkinbinds:
    Gui, 2:Destroy
    Gui, 2:Add, Text, x62 y9 w110 h30 , Set Boomkin binds
    ;get current binds
    FileReadLine, boomybinds, moonkin\binds.txt, 1
    Wrath:=linewithjustskill(boomybinds)
    FileReadLine, boomybinds, moonkin\binds.txt, 2
    Starblast:=linewithjustskill(boomybinds)
    FileReadLine, boomybinds, moonkin\binds.txt, 3
    Swarm:=linewithjustskill(boomybinds)
    FileReadLine, boomybinds, moonkin\binds.txt, 4
    Moonfire:=linewithjustskill(boomybinds)


    Gui, 2:Add, Hotkey, x102 y49 w80 h30  vWrath2 , %Wrath%
    Gui, 2:Add, Hotkey, x102 y89 w80 h30 vStarblast2, %Starblast%
    Gui, 2:Add, Hotkey, x102 y129 w80 h30 vSwarm2, %Swarm%
    Gui, 2:Add, Hotkey, x102 y169 w80 h30 vMoonfire2, %Moonfire%
    Gui, 2:Add, Text, x22 y54 w70 h30 , Wrath
    Gui, 2:Add, Text, x-8 y659 w130 h-560 , Text
    Gui, 2:Add, Text, x22 y95 w70 h30 , Star Blast
    Gui, 2:Add, Text, x22 y134 w70 h30 , Insect Swarm
    Gui, 2:Add, Text, x22 y174 w70 h30 , Moonfire

    Gui, 2:Add, Button, x102 y349 w80 h30 gcancel4 , Cancel
    Gui, 2:Add, Button, x22 y349 w70 h30  gapply4, Apply
    ; Generated using SmartGUI Creator 4.0
    Gui, 2:Show, x774 y366 h408 w211, Boomkin binds
    return

    cancel4:
        ExitApp
        return
    apply4:
        Gui, Submit
        l:=StrLen(Wrath2)
        if l>0:
            replace(Wrath2,1,"moonkin\binds.txt")
        l:=StrLen(Starblast2)
        if l>0:
            replace(Starblast2,2,"moonkin\binds.txt")
        l:=StrLen(Swarm2)
        if l>0:
            replace(Swarm2,3,"moonkin\binds.txt")
        l:=StrLen(Moonfire2)
        if l>0:
            replace(Moonfire2,4,"moonkin\binds.txt")
        return
magebinds:
    Gui, 2:Destroy
    Gui, 2:Add, Text, x62 y9 w110 h30 , Set Mage binds
    ;get current binds
    FileReadLine, magebinds, mage\binds.txt, 1
    Fireball:=linewithjustskill(magebinds)
    FileReadLine, magebinds, mage\binds.txt, 2
    Livingbomb:=linewithjustskill(magebinds)
    FileReadLine, magebinds, mage\binds.txt, 3
    Pyro:=linewithjustskill(magebinds)
    FileReadLine, magebinds, mage\binds.txt, 4
    Scorch:=linewithjustskill(magebinds)


    Gui, 2:Add, Hotkey, x102 y49 w80 h30  vFireball2 , %Fireball%
    Gui, 2:Add, Hotkey, x102 y89 w80 h30 vLivingbomb2, %Livingbomb%
    Gui, 2:Add, Hotkey, x102 y129 w80 h30 vPyro2, %Pyro%
    Gui, 2:Add, Hotkey, x102 y169 w80 h30 vScorch2, %Scorch%
    Gui, 2:Add, Text, x22 y54 w70 h30 , Fireball
    Gui, 2:Add, Text, x-8 y659 w130 h-560 , Text
    Gui, 2:Add, Text, x22 y95 w70 h30 , Living Bomb
    Gui, 2:Add, Text, x22 y134 w70 h30 , Pyroball
    Gui, 2:Add, Text, x22 y174 w70 h30 , Scorch

    Gui, 2:Add, Button, x102 y349 w80 h30 gcancel5 , Cancel
    Gui, 2:Add, Button, x22 y349 w70 h30  gapply5, Apply
    ; Generated using SmartGUI Creator 4.0
    Gui, 2:Show, x774 y366 h408 w211, Mage binds
    return

    cancel5:
        ExitApp
        return
    apply5:
        Gui, Submit
        l:=StrLen(Fireball2)
        if l>0:
            replace(Fireball2,1,"mage\binds.txt")
        l:=StrLen(Livingbomb2)
        if l>0:
            replace(Livingbomb2,2,"mage\binds.txt")
        l:=StrLen(Pyro2)
        if l>0:
            replace(Pyro2,3,"mage\binds.txt")
        l:=StrLen(Scorch2)
        if l>0:
            replace(Scorch2,4,"mage\binds.txt")
        return
unholybinds:
    Gui, 2:Destroy
    Gui, 2:Add, Text, x62 y9 w110 h30 , Set Unholy DK binds
    ;get current binds
    FileReadLine, unholybinds, dk pics\binds.txt, 1
    Bloodstrike:=linewithjustskill(unholybinds)
    FileReadLine, unholybinds, dk pics\binds.txt, 2
    Scourge:=linewithjustskill(unholybinds)
    FileReadLine, unholybinds, dk pics\binds.txt, 3
    Deathcoil:=linewithjustskill(unholybinds)
    FileReadLine, unholybinds, dk pics\binds.txt, 4
    Icy:=linewithjustskill(unholybinds)
    FileReadLine, unholybinds, dk pics\binds.txt, 5
    Plague:=linewithjustskill(unholybinds)
    FileReadLine, unholybinds, dk pics\binds.txt, 6
    Pestilence:=linewithjustskill(unholybinds)
    FileReadLine, unholybinds, dk pics\binds.txt, 7
    Shield:=linewithjustskill(unholybinds)
    FileReadLine, unholybinds, dk pics\binds.txt, 8
    Horn:=linewithjustskill(unholybinds)
    FileReadLine, unholybinds, dk pics\binds.txt, 9
    Gargoyle:=linewithjustskill(unholybinds)

    Gui, 2:Add, Hotkey, x102 y49 w80 h30  vBloodstrike2 , %Bloodstrike%
    Gui, 2:Add, Hotkey, x102 y89 w80 h30 vScourge2, %Scourge%
    Gui, 2:Add, Hotkey, x102 y129 w80 h30 vDeathcoil2, %Deathcoil%
    Gui, 2:Add, Hotkey, x102 y169 w80 h30 vIcy2, %Icy%
    Gui, 2:Add, Hotkey, x102 y209 w80 h30 vPlague2, %Plague%
    Gui, 2:Add, Hotkey, x102 y249 w80 h30 vPestilence2, %Pestilence%
    Gui, 2:Add, Hotkey, x102 y289 w80 h30 vShield2, %Shield%
    Gui, 2:Add, Hotkey, x102 y329 w80 h30 vHorn2, %Horn%
    Gui, 2:Add, Hotkey, x102 y369 w80 h30 vGargoyle2, %Gargoyle%
    Gui, 2:Add, Text, x22 y54 w70 h30 , Blood Strike
    Gui, 2:Add, Text, x-8 y659 w130 h-560 , Text
    Gui, 2:Add, Text, x22 y95 w70 h30 , Scourge Strike
    Gui, 2:Add, Text, x22 y134 w70 h30 , Death Coil
    Gui, 2:Add, Text, x22 y174 w70 h30 , Icy Touch
    Gui, 2:Add, Text, x22 y214 w70 h30 , Plague Strike
    Gui, 2:Add, Text, x22 y254 w70 h30 , Pestilence
    Gui, 2:Add, Text, x22 y294 w70 h30 , Bone Shield
    Gui, 2:Add, Text, x22 y334 w70 h30 , Horn of Winter
    Gui, 2:Add, Text, x22 y374 w70 h30 , Ebon Gargoyle
    Gui, 2:Add, Button, x102 y419 w80 h30 gcancel6 , Cancel
    Gui, 2:Add, Button, x22 y419 w70 h30  gapply6, Apply
    ; Generated using SmartGUI Creator 4.0
    Gui, 2:Show, x774 y366 h458 w211, Unholy dk binds
    return

    cancel6:
        ExitApp
        return
    apply6:
        Gui, Submit
        l:=StrLen(Bloodstrike2)
        if l>0:
            replace(Bloodstrike2,1,"dk pics\binds.txt")
        l:=StrLen(Scourge2)
        if l>0:
            replace(Scourge2,2,"dk pics\binds.txt")
        l:=StrLen(Deathcoil2)
        if l>0:
            replace(Deathcoil2,3,"dk pics\binds.txt")
        l:=StrLen(Icy2)
        if l>0:
            replace(Icy2,4,"dk pics\binds.txt")
        l:=StrLen(Plague2)
        if l>0:
            replace(Plague2,5,"dk pics\binds.txt")
        l:=StrLen(Pestilence2)
        if l>0:
            replace(Pestilence2,6,"dk pics\binds.txt")
        l:=StrLen(Shield2)
        if l>0:
            replace(Shield2,7,"dk pics\binds.txt")
        l:=StrLen(Horn2)
        if l>0:
            replace(Horn2,8,"dk pics\binds.txt")
        l:=StrLen(Gargoyle2)
        if l>0:
            replace(Gargoyle2,9,"dk pics\binds.txt")
        return
frostbinds:
    Gui, 2:Destroy
    Gui, 2:Add, Text, x62 y9 w110 h30 , Set Frost DK binds
    ;get current binds
    FileReadLine, frostbinds, dk pics\binds.txt, 1
    Bloodstrike:=linewithjustskill(frostbinds)
    FileReadLine, frostbinds, dk pics\binds.txt, 10
    Obliterate:=linewithjustskill(frostbinds)
    FileReadLine, frostbinds, dk pics\binds.txt, 11
    FrostStrike:=linewithjustskill(frostbinds)
    FileReadLine, frostbinds, dk pics\binds.txt, 4
    Icy:=linewithjustskill(frostbinds)
    FileReadLine, frostbinds, dk pics\binds.txt, 5
    Plague:=linewithjustskill(frostbinds)
    FileReadLine, frostbinds, dk pics\binds.txt, 6
    Pestilence:=linewithjustskill(frostbinds)
    FileReadLine, frostbinds, dk pics\binds.txt, 12
    Howling:=linewithjustskill(frostbinds)
    FileReadLine, unholybinds, dk pics\binds.txt, 8
    Horn:=linewithjustskill(unholybinds)

    Gui, 2:Add, Hotkey, x102 y49 w80 h30  vBloodstrike2 , %Bloodstrike%
    Gui, 2:Add, Hotkey, x102 y89 w80 h30 vObliterate2, %Obliterate%
    Gui, 2:Add, Hotkey, x102 y129 w80 h30 vFrostStrike2, %FrostStrike%
    Gui, 2:Add, Hotkey, x102 y169 w80 h30 vIcy2, %Icy%
    Gui, 2:Add, Hotkey, x102 y209 w80 h30 vPlague2, %Plague%
    Gui, 2:Add, Hotkey, x102 y249 w80 h30 vPestilence2, %Pestilence%
    Gui, 2:Add, Hotkey, x102 y289 w80 h30 vHowling2, %Howling%
    Gui, 2:Add, Hotkey, x102 y329 w80 h30 vHorn2, %Horn%

    Gui, 2:Add, Text, x22 y54 w70 h30 , Blood Strike
    Gui, 2:Add, Text, x-8 y659 w130 h-560 , Text
    Gui, 2:Add, Text, x22 y95 w70 h30 , Obliterate
    Gui, 2:Add, Text, x22 y134 w70 h30 , FrostStrike
    Gui, 2:Add, Text, x22 y174 w70 h30 , Icy Touch
    Gui, 2:Add, Text, x22 y214 w70 h30 , Plague Strike
    Gui, 2:Add, Text, x22 y254 w70 h30 , Pestilence
    Gui, 2:Add, Text, x22 y294 w70 h30 , Howling Blast
    Gui, 2:Add, Text, x22 y334 w70 h30 , Horn of Winter
    Gui, 2:Add, Button, x102 y419 w80 h30 gcancel7 , Cancel
    Gui, 2:Add, Button, x22 y419 w70 h30  gapply7, Apply
    ; Generated using SmartGUI Creator 4.0
    Gui, 2:Show, x774 y366 h458 w211, Frost dk binds
    return

    cancel7:
        ExitApp
        return
    apply7:
        Gui, Submit
        l:=StrLen(Bloodstrike2)
        if l>0:
            replace(Bloodstrike2,1,"dk pics\binds.txt")
        l:=StrLen(Obliterate2)
        if l>0:
            replace(Obliterate2,10,"dk pics\binds.txt")
        l:=StrLen(FrostStrike2)
        if l>0:
            replace(FrostStrike2,11,"dk pics\binds.txt")
        l:=StrLen(Icy2)
        if l>0:
            replace(Icy2,4,"dk pics\binds.txt")
        l:=StrLen(Plague2)
        if l>0:
            replace(Plague2,5,"dk pics\binds.txt")
        l:=StrLen(Pestilence2)
        if l>0:
            replace(Pestilence2,6,"dk pics\binds.txt")
        l:=StrLen(Howling2)
        if l>0:
            replace(Howling2,12,"dk pics\binds.txt")
        l:=StrLen(Horn2)
        if l>0:
            replace(Horn2,8,"dk pics\binds.txt")
        return
bloodbinds:
    Gui, 2:Destroy
    Gui, 2:Add, Text, x62 y9 w110 h30 , Set Blood DK binds
    ;get current binds
    FileReadLine, bloodbinds, dk pics\binds.txt, 14
    Heartstrike:=linewithjustskill(bloodbinds)
    FileReadLine, bloodbinds, dk pics\binds.txt, 13
    DeathStrike:=linewithjustskill(bloodbinds)
    FileReadLine, bloodbinds, dk pics\binds.txt, 3
    Deathcoil:=linewithjustskill(bloodbinds)
    FileReadLine, bloodbinds, dk pics\binds.txt, 4
    Icy:=linewithjustskill(bloodbinds)
    FileReadLine, bloodbinds, dk pics\binds.txt, 5
    Plague:=linewithjustskill(bloodbinds)
    FileReadLine, bloodbinds, dk pics\binds.txt, 6
    Pestilence:=linewithjustskill(bloodbinds)
    FileReadLine, bloodbinds, dk pics\binds.txt, 12
    DRW:=linewithjustskill(bloodbinds)
    FileReadLine, unholybinds, dk pics\binds.txt, 15
    Horn:=linewithjustskill(unholybinds)

    Gui, 2:Add, Hotkey, x102 y49 w80 h30  vHeartStrike2 , %Heartstrike%
    Gui, 2:Add, Hotkey, x102 y89 w80 h30 vDeathStrike2, %DeathStrike%
    Gui, 2:Add, Hotkey, x102 y129 w80 h30 vDeathcoil2, %Deathcoil%
    Gui, 2:Add, Hotkey, x102 y169 w80 h30 vIcy2, %Icy%
    Gui, 2:Add, Hotkey, x102 y209 w80 h30 vPlague2, %Plague%
    Gui, 2:Add, Hotkey, x102 y249 w80 h30 vPestilence2, %Pestilence%
    Gui, 2:Add, Hotkey, x102 y289 w80 h30 vDRW2, %DRW%
    Gui, 2:Add, Hotkey, x102 y329 w80 h30 vHorn2, %Horn%

    Gui, 2:Add, Text, x22 y54 w70 h30 , Heart Strike
    Gui, 2:Add, Text, x-8 y659 w130 h-560 , Text
    Gui, 2:Add, Text, x22 y95 w70 h30 , Death Strike
    Gui, 2:Add, Text, x22 y134 w70 h30 , Death coil
    Gui, 2:Add, Text, x22 y174 w70 h30 , Icy Touch
    Gui, 2:Add, Text, x22 y214 w70 h30 , Plague Strike
    Gui, 2:Add, Text, x22 y254 w70 h30 , Pestilence
    Gui, 2:Add, Text, x22 y294 w70 h30 , Dancing Rune Weapon
    Gui, 2:Add, Text, x22 y334 w70 h30 , Horn of Winter
    Gui, 2:Add, Button, x102 y419 w80 h30 gcancel8 , Cancel
    Gui, 2:Add, Button, x22 y419 w70 h30  gapply8, Apply
    ; Generated using SmartGUI Creator 4.0
    Gui, 2:Show, x774 y366 h458 w211, Blood dk binds
    return

    cancel8:
        ExitApp
        return
    apply8:
        Gui, Submit
        l:=StrLen(HeartStrike2)
        if l>0:
            replace(HeartStrike2,14,"dk pics\binds.txt")
        l:=StrLen(DeathStrike2)
        if l>0:
            replace(DeathStrike2,13,"dk pics\binds.txt")
        l:=StrLen(Deathcoil2)
        if l>0:
            replace(Deathcoil2,3,"dk pics\binds.txt")
        l:=StrLen(Icy2)
        if l>0:
            replace(Icy2,4,"dk pics\binds.txt")
        l:=StrLen(Plague2)
        if l>0:
            replace(Plague2,5,"dk pics\binds.txt")
        l:=StrLen(Pestilence2)
        if l>0:
            replace(Pestilence2,6,"dk pics\binds.txt")
        l:=StrLen(DRW2)
        if l>0:
            replace(DRW2,12,"dk pics\binds.txt")
        l:=StrLen(Horn2)
        if l>0:
            replace(Horn2,15,"dk pics\binds.txt")
        return
hunterbinds:
    Gui, 2:Destroy
    Gui, 2:Add, Text, x62 y9 w110 h30 , Set Hunter binds
    ;get current binds
    FileReadLine, hunterbinds, hunter\binds.txt, 1
    Aimed:=linewithjustskill(hunterbinds)
    FileReadLine, hunterbinds, hunter\binds.txt, 2
    Chimera:=linewithjustskill(hunterbinds)
    FileReadLine, hunterbinds, hunter\binds.txt, 3
    Mark:=linewithjustskill(hunterbinds)
    FileReadLine, hunterbinds, hunter\binds.txt, 4
    Steady:=linewithjustskill(hunterbinds)
    FileReadLine, hunterbinds, hunter\binds.txt, 5
    Serpent:=linewithjustskill(hunterbinds)
    FileReadLine, hunterbinds, hunter\binds.txt, 6
    Killing:=linewithjustskill(hunterbinds)


    Gui, 2:Add, Hotkey, x102 y49 w80 h30  vAimed2 , %Aimed%
    Gui, 2:Add, Hotkey, x102 y89 w80 h30 vChimera2, %Chimera%
    Gui, 2:Add, Hotkey, x102 y129 w80 h30 vMark2, %Mark%
    Gui, 2:Add, Hotkey, x102 y169 w80 h30 vSteady2, %Steady%
    Gui, 2:Add, Hotkey, x102 y209 w80 h30 vSerpent2, %Serpent%
    Gui, 2:Add, Hotkey, x102 y249 w80 h30 vKilling2, %Killing%
    Gui, 2:Add, Text, x22 y54 w70 h30 , Aimed Shot
    Gui, 2:Add, Text, x-8 y659 w130 h-560 , Text
    Gui, 2:Add, Text, x22 y95 w70 h30 , Chimera Shot
    Gui, 2:Add, Text, x22 y134 w70 h30 , Hunters Mark
    Gui, 2:Add, Text, x22 y174 w70 h30 , Steady Shot
    Gui, 2:Add, Text, x22 y214 w70 h30 , Serpent Sting
     Gui, 2:Add, Text, x22 y254 w70 h30 , Killing Shot

    Gui, 2:Add, Button, x102 y349 w80 h30 gcancel9 , Cancel
    Gui, 2:Add, Button, x22 y349 w70 h30  gapply9, Apply
    ; Generated using SmartGUI Creator 4.0
    Gui, 2:Show, x774 y366 h408 w211, Hunter binds
    return

    cancel9:
        ExitApp
        return
    apply9:
        Gui, Submit
        l:=StrLen(Aimed2)
        if l>0:
            replace(Aimed2,1,"hunter\binds.txt")
        l:=StrLen(Chimera2)
        if l>0:
            replace(Chimera2,2,"hunter\binds.txt")
        l:=StrLen(Mark2)
        if l>0:
            replace(Mark2,3,"hunter\binds.txt")
        l:=StrLen(Steady2)
        if l>0:
            replace(Steady2,4,"hunter\binds.txt")
        l:=StrLen(Serpent2)
        if l>0:
            replace(Serpent2,5,"hunter\binds.txt")
        l:=StrLen(Killing2)
        if l>0:
            replace(Killing2,6,"hunter\binds.txt")
        return
demobinds:
    Gui, 2:Destroy
    Gui, 2:Add, Text, x62 y9 w110 h30 , Set Demo Lock binds
    ;get current binds
    FileReadLine, bloodbinds, warlock\binds.txt, 1
    Corruption:=linewithjustskill(bloodbinds)
    FileReadLine, bloodbinds, warlock\binds.txt, 2
    Curse:=linewithjustskill(bloodbinds)
    FileReadLine, bloodbinds, warlock\binds.txt, 3
    Soulfire:=linewithjustskill(bloodbinds)
    FileReadLine, bloodbinds, warlock\binds.txt, 4
    Immolate:=linewithjustskill(bloodbinds)
    FileReadLine, bloodbinds, warlock\binds.txt, 5
    Incirenate:=linewithjustskill(bloodbinds)
    FileReadLine, bloodbinds, warlock\binds.txt, 6
    Shadowbolt:=linewithjustskill(bloodbinds)
    FileReadLine, bloodbinds, warlock\binds.txt, 7
    Lifetap:=linewithjustskill(bloodbinds)


    Gui, 2:Add, Hotkey, x102 y49 w80 h30  vCorruption2 , %Corruption%
    Gui, 2:Add, Hotkey, x102 y89 w80 h30 vCurse2, %Curse%
    Gui, 2:Add, Hotkey, x102 y129 w80 h30 vSoulfire2, %Soulfire%
    Gui, 2:Add, Hotkey, x102 y169 w80 h30 vImmolate2, %Immolate%
    Gui, 2:Add, Hotkey, x102 y209 w80 h30 vIncirenate2, %Incirenate%
    Gui, 2:Add, Hotkey, x102 y249 w80 h30 vShadowbolt2, %Shadowbolt%
    Gui, 2:Add, Hotkey, x102 y289 w80 h30 vLifetap2, %Lifetap%

    Gui, 2:Add, Text, x22 y54 w70 h30 , Corruption
    Gui, 2:Add, Text, x-8 y659 w130 h-560 , Text
    Gui, 2:Add, Text, x22 y95 w70 h30 , Curse
    Gui, 2:Add, Text, x22 y134 w70 h30 , Soul Fire
    Gui, 2:Add, Text, x22 y174 w70 h30 , Immolate
    Gui, 2:Add, Text, x22 y214 w70 h30 , Incirenate
    Gui, 2:Add, Text, x22 y254 w70 h30 , Shadow Bolt
    Gui, 2:Add, Text, x22 y294 w70 h30 , Life Tap
    Gui, 2:Add, Button, x102 y419 w80 h30 gcancel10 , Cancel
    Gui, 2:Add, Button, x22 y419 w70 h30  gapply10, Apply
    ; Generated using SmartGUI Creator 4.0
    Gui, 2:Show, x774 y366 h458 w211, Demo Lock binds
    return

    cancel10:
        ExitApp
        return
    apply10:
        Gui, Submit
        l:=StrLen(Corruption2)
        if l>0:
            replace(Corruption2,1,"warlock\binds.txt")
        l:=StrLen(Curse2)
        if l>0:
            replace(Curse2,2,"warlock\binds.txt")
        l:=StrLen(Soulfire2)
        if l>0:
            replace(Soulfire2,3,"warlock\binds.txt")
        l:=StrLen(Immolate2)
        if l>0:
            replace(Immolate2,4,"warlock\binds.txt")
        l:=StrLen(Incirenate2)
        if l>0:
            replace(Incirenate2,5,"warlock\binds.txt")
        l:=StrLen(Shadowbolt2)
        if l>0:
            replace(Shadowbolt2,6,"warlock\binds.txt")
        l:=StrLen(Lifetap2)
        if l>0:
            replace(Lifetap2,7,"warlock\binds.txt")
        return

roguebinds:
    Gui, 2:Destroy
    Gui, 2:Add, Text, x62 y9 w110 h30 , Set Rogue binds
    ;get current binds
    FileReadLine, roguebinds, rogue\binds.txt, 1
    Sinister:=linewithjustskill(roguebinds)
    FileReadLine, roguebinds, rogue\binds.txt, 2
    Rupture:=linewithjustskill(roguebinds)
    FileReadLine, roguebinds, rogue\binds.txt, 3
    Slice:=linewithjustskill(roguebinds)
    FileReadLine, roguebinds, rogue\binds.txt, 4
    Eviscerate:=linewithjustskill(roguebinds)
    FileReadLine, roguebinds, rogue\binds.txt, 5
    FanOfKnives:=linewithjustskill(roguebinds)



    Gui, 2:Add, Hotkey, x102 y49 w80 h30  vSinister2 , %Sinister%
    Gui, 2:Add, Hotkey, x102 y89 w80 h30 vRupture2, %Rupture%
    Gui, 2:Add, Hotkey, x102 y129 w80 h30 vSlice2, %Slice%
    Gui, 2:Add, Hotkey, x102 y169 w80 h30 vEviscerate2, %Eviscerate%
    Gui, 2:Add, Hotkey, x102 y209 w80 h30 vFanOfKnives2, %FanOfKnives%
    
    Gui, 2:Add, Text, x22 y54 w70 h30 , Sinister Strike
    Gui, 2:Add, Text, x-8 y659 w130 h-560 , Text
    Gui, 2:Add, Text, x22 y95 w70 h30 , Rupture
    Gui, 2:Add, Text, x22 y134 w70 h30 , Slice and Dice
    Gui, 2:Add, Text, x22 y174 w70 h30 , Eviscerate
    Gui, 2:Add, Text, x22 y214 w70 h30 , Fan of Knives
    Gui, 2:Add, Button, x102 y419 w80 h30 gcancel11 , Cancel
    Gui, 2:Add, Button, x22 y419 w70 h30  gapply11, Apply
    ; Generated using SmartGUI Creator 4.0
    Gui, 2:Show, x774 y366 h458 w211, Rogue binds
    return

    cancel11:
        ExitApp
        return
    apply11:
        Gui, Submit
        l:=StrLen(Sinister2)
        if l>0:
            replace(Sinister2,1,"rogue\binds.txt")
        l:=StrLen(Rupture2)
        if l>0:
            replace(Rupture2,2,"rogue\binds.txt")
        l:=StrLen(Slice2)
        if l>0:
            replace(Slice2,3,"rogue\binds.txt")
        l:=StrLen(Eviscerate2)
        if l>0:
            replace(Eviscerate2,4,"rogue\binds.txt")
        l:=StrLen(FanOfKnives2)
        if l>0:
            replace(FanOfKnives2,5,"rogue\binds.txt")

        return
    
priestbinds:
    Gui, 2:Destroy
    Gui, 2:Add, Text, x62 y9 w110 h30 , Set Priest binds
    ;get current binds
    FileReadLine, priestbinds, priest\binds.txt, 1
    VampiricTouch:=linewithjustskill(priestbinds)
    FileReadLine, priestbinds, priest\binds.txt, 2
    DevouringPlague:=linewithjustskill(priestbinds)
    FileReadLine, priestbinds, priest\binds.txt, 3
    MindBlast:=linewithjustskill(priestbinds)
    FileReadLine, priestbinds, priest\binds.txt, 4
    ShadowWordPain:=linewithjustskill(priestbinds)
    FileReadLine, priestbinds, priest\binds.txt, 5
    MindFlay:=linewithjustskill(priestbinds)



    Gui, 2:Add, Hotkey, x102 y49 w80 h30  vVampiricTouch2 , %VampiricTouch%
    Gui, 2:Add, Hotkey, x102 y89 w80 h30 vDevouringPlague2, %DevouringPlague%
    Gui, 2:Add, Hotkey, x102 y129 w80 h30 vMindBlast2, %MindBlast%
    Gui, 2:Add, Hotkey, x102 y169 w80 h30 vShadowWordPain2, %ShadowWordPain%
    Gui, 2:Add, Hotkey, x102 y209 w80 h30 vMindFlay2, %MindFlay%

    Gui, 2:Add, Text, x22 y54 w70 h30 , Vampiric Touch
    Gui, 2:Add, Text, x-8 y659 w130 h-560 , Text
    Gui, 2:Add, Text, x22 y95 w70 h30 , Devouring Plague
    Gui, 2:Add, Text, x22 y134 w70 h30 , Mind Blast
    Gui, 2:Add, Text, x22 y174 w70 h30 , Shadow Word Pain
    Gui, 2:Add, Text, x22 y214 w70 h30 , Mind Flay
    Gui, 2:Add, Button, x102 y419 w80 h30 gcancel12 , Cancel
    Gui, 2:Add, Button, x22 y419 w70 h30  gapply12, Apply
    ; Generated using SmartGUI Creator 4.0
    Gui, 2:Show, x774 y366 h458 w211, Priest binds
    return

    cancel12:
        ExitApp
        return
    apply12:
        Gui, Submit
        l:=StrLen(VampiricTouch2)
        if l>0:
            replace(VampiricTouch2,1,"priest\binds.txt")
        l:=StrLen(DevouringPlague2)
        if l>0:
            replace(DevouringPlague2,2,"priest\binds.txt")
        l:=StrLen(MindBlast2)
        if l>0:
            replace(MindBlast2,3,"priest\binds.txt")
        l:=StrLen(ShadowWordPain2)
        if l>0:
            replace(ShadowWordPain2,4,"priest\binds.txt")
        l:=StrLen(MindFlay2)
        if l>0:
            replace(MindFlay2,5,"priest\binds.txt")

        return



return
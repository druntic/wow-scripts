scriptPath := A_WorkingDir . "\scripts.txt"
FileRead, fileContent, %scriptPath%
scripts:=StrSplit(fileContent, ",")
currentscript:=SubStr(A_ScriptName, 1, -4)
Loop % scripts.Length(){
    value:=scripts[A_Index]
    if(value != currentscript) 
        WinClose, %value%.ahk
}
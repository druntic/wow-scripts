
#Include, ./setting/getImageSize.ahk
Nfirstlines(n,location){
    i:=1

    while(i<= n){
        FileReadLine, binds, %location%, %i%
        i++
        var=%var% `n %binds%
    }
    return var
}
Nrestlines(n,total_lines,location){
    i:=0
    while(i< n){
        diff:=total_lines-i
        ;MsgBox, %diff%
        FileReadLine, binds, %location%, %diff%
        i++
        var= %binds% `n %var%
    }
    return var
}

linewithoutskill(line){
    pos1:=InStr(line, ":")
    StringTrimRight, x, line, StrLen(line)-pos1
    return x
}
linewithjustskill(line){
    pos1:=InStr(line, ":")
    StringTrimLeft, x, line, pos1
    return x
}
x:=linewithjustskill("bloodthirst:1")
;MsgBox, %x%
;MsgBox, %var%%var2%

replace(newbind, line, location){
    FileReadLine, binds, %location%, line
    ;StringLeft, bind, binds, StrLen(binds)-1 ; mora se jos popraviti da ne uzima zadnji char nego sve nakon znaka ":"
    bind:=linewithoutskill(binds)
    ;MsgBox, %bind%
    if newbind is Integer
        newskill= %bind%%newbind%
    Else
        newskill= %bind% %newbind%
    ;MsgBox, %newskill%
    Loop, Read, %location%
    {
        total_lines = %A_Index%
    }

    var:=Nfirstlines(line-1,location)
    var2 := Nrestlines(total_lines-line+1,total_lines,location)
    var3:=SubStr(var2, InStr(var2,"`n") + 1)
    var4=%newskill% `n %var3%
    var5=%var% `n %var4%
    var5:=SubStr(var5, InStr(var5,"`n") + 1)
    ;MsgBox, %newskill%
    StringReplace, var5, var5, %A_SPACE%,, All

    FileDelete, %location%
    FileAppend, %var5%, %location%
}

;tooltip when hovering over help icon
WM_MOUSEMOVE()
{
    static CurrControl, PrevControl, _TT ; _TT is kept blank for use by the ToolTip command below.
    CurrControl := A_GuiControl
    If (CurrControl <> PrevControl and not InStr(CurrControl, " "))
    {
        ToolTip ; Turn off any previous tooltip.
        SetTimer, DisplayToolTip, 1000
        PrevControl := CurrControl
    }
    return

    DisplayToolTip:
        SetTimer, DisplayToolTip, Off
        ToolTip % %CurrControl%_TT ; The leading percent sign tell it to use an expression.
        ;SetTimer, RemoveToolTip, 3000
    return

    RemoveToolTip:
        SetTimer, RemoveToolTip, Off
        ToolTip
    return
}

saveImageCoordinates(x1, y1, width, height , path, lineNumber) {
    ; Calculate bottom-right coordinates
    x2 := x1 + width
    y2 := y1 + height

    ; Prepare the line to write
    newLine := x1 ", " y1 ", " x2 ", " y2 . "`n"
    ; Read the existing content 
    FileRead, fileContent, %path%

    ; Split the content into lines
    StringSplit, lines, fileContent, `n

    ; Replace the first line with the new data
    lines%lineNumber% := newLine
    ; Combine the lines back into a single string
    newContent := ""
    if(fileContent = "") {
        newContent := newLine 
    }
    else {
        Loop, % lineNumber + 1 ;lines0 is the total number of lines in fileContent
        {
            newContent .= lines%A_Index%  
        }
    }
    ; Write the new content back to the file
    FileDelete, %path%
    FileAppend, %newContent%, %path%
}

searchAndSavePictures(pictureArray, pathToSave) {
    index := 1
    length := pictureArray.MaxIndex()
    for key, value in pictureArray
    {
        MsgBox, Please show the %value% picture
        ;picturePath will have to be changed when added  to main.ahk. probably path too
        picturePath := A_ScriptDir "\" A_ScreenHeight "\" value ".png "
        picture := LoadPicture(picturePath)
        imgSize(picturePath, pictureWidth , pictureHeight)
        Loop, {
            ImageSearch, x, y, 0, 0, A_ScreenWidth, A_ScreenHeight, *50 HBITMAP:*%picture%
            if (ErrorLevel = 0) {
                MsgBox, %index%/%length% %value% picture found! Saving coordinates in line %index%
                saveImageCoordinates(x, y, pictureWidth, pictureHeight , pathToSave, index)
                break
            }
        }
        index++
    }
    MsgBox, All images found. Now exiting calibrator.
    ExitApp
}
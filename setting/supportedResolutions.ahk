supportedResolutions = [1080,1440]
if (!(A_ScreenHeight in supportedResolutions)) {
    MsgBox, "Currently supported resolutions are 1080 and 1440"
    ExitApp
}
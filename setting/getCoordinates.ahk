; Default coordinates
x1 := 636
y1 := 570
x2 := 760
y2 := 695

; Path to the coordinates file
FilePath := A_ScriptDir "\boxCoordinates.txt"
; Check if the file exists and read coordinates if it does
if FileExist(FilePath) {
    FileRead, FileContent, %FilePath%
    ; Example format:
    ; x1
    ; y1
    ; x2
    ; y2
    StringSplit, coords, FileContent, `n
    ; Ensure we have the right number of coordinates
    if (coords0 >= 4) {
        x1 := StrReplace(coords1, "`r", "")
        y1 := StrReplace(coords2, "`r", "")
        x2 := StrReplace(coords3, "`r", "")
        y2 := StrReplace(coords4, "`r", "")
    }
}
#SingleInstance Force
#Include, setting\getCoordinates.ahk
Gui, Color, Red
Gui, +AlwaysOnTop -Caption +ToolWindow +E0x20 ; E0x20 makes it click-through by default

; Calculate width and height
UpdateDimensions()
Gui, Show, x%x1% y%y1% w%w3% h%h3%
Gui, +LastFound
WinSet, TransColor, feffff 126

; Variables to track resizing and dragging
Resizing := false
Dragging := false
Corner := ""
OffsetX := 0
OffsetY := 0

~LButton::
    ; Make the window non-click-through when interacting
    Gui, -E0x20 ; Disable click-through

    MouseGetPos, MouseX, MouseY
    ; Check if clicking near the corners for resizing
    if (MouseX >= x1-10 and MouseX <= x1+10 and MouseY >= y1-10 and MouseY <= y1+10) {
        Corner := "TopLeft"
        Resizing := true
    } else if (MouseX >= x2-10 and MouseX <= x2+10 and MouseY >= y1-10 and MouseY <= y1+10) {
        Corner := "TopRight"
        Resizing := true
    } else if (MouseX >= x1-10 and MouseX <= x1+10 and MouseY >= y2-10 and MouseY <= y2+10) {
        Corner := "BottomLeft"
        Resizing := true
    } else if (MouseX >= x2-10 and MouseX <= x2+10 and MouseY >= y2-10 and MouseY <= y2+10) {
        Corner := "BottomRight"
        Resizing := true
    } else if (MouseX >= x1 and MouseX <= x2 and MouseY >= y1 and MouseY <= y2) {
        ; If clicking within the square, initiate dragging
        Dragging := true
        OffsetX := MouseX - x1
        OffsetY := MouseY - y1
    }

    ; Start the timer to monitor mouse movement
    if (Resizing or Dragging) {
        SetTimer, UpdateGuiPositionOrSize, 10
    }
Return

~LButton Up::
    ; Stop resizing/dragging and stop the timer
    Resizing := false
    Dragging := false
    SetTimer, UpdateGuiPositionOrSize, Off

    ; Restore click-through functionality
    Gui, +E0x20 ; Enable click-through
Return

UpdateGuiPositionOrSize:
    MouseGetPos, MouseX, MouseY
    if (Resizing) {
        ; Handle resizing
        if (Corner = "TopLeft") {
            x1 := MouseX
            y1 := MouseY
        } else if (Corner = "TopRight") {
            x2 := MouseX
            y1 := MouseY
        } else if (Corner = "BottomLeft") {
            x1 := MouseX
            y2 := MouseY
        } else if (Corner = "BottomRight") {
            x2 := MouseX
            y2 := MouseY
        }
        ; Update the width and height
        UpdateDimensions()
        ; Update the GUI size and position
        Gui, Show, x%x1% y%y1% w%w3% h%h3% NoActivate
    } else if (Dragging) {
        ; Handle dragging
        x1 := MouseX - OffsetX
        y1 := MouseY - OffsetY
        x2 := x1 + w3
        y2 := y1 + h3
        ; Update the GUI position
        Gui, Show, x%x1% y%y1% NoActivate
    }
Return

UpdateDimensions() {
    global x1, y1, x2, y2, w3, h3
    ; Calculate width and height
    w3 := x2 - x1
    h3 := y2 - y1
    ; Debugging message
    ToolTip, x1: %x1%`ny1: %y1%`nx2: %x2%`ny2: %y2%`nw3: %w3%`nh3: %h3%
}

; F1 key to exit the script
F1::ExitApp

; F2 key to save coordinates to a file
F2::
    ; Delete the existing file (clears old content)
    if FileExist(FilePath)
        FileDelete, %FilePath%

    ; Save each coordinate on a new line
    FileAppend, %x1%`n, %FilePath%
    FileAppend, %y1%`n, %FilePath%
    FileAppend, %x2%`n, %FilePath%
    FileAppend, %y2%`n, %FilePath%
    MsgBox, 64, Coordinates Saved, The coordinates have been saved to `n` %FilePath%
Return

GuiEscape:
GuiClose:
ExitApp

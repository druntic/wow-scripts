#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\feral\getFeralCoordinates.ahk
supportedResolutions = [1080,1440]
if (!(A_ScreenHeight in supportedResolutions)) {
    MsgBox, "Currently supported resolutions are 1080 and 1440"
    ExitApp
}
FileReadLine, feralbinds, feral\binds.txt, 1
Mangle:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 2
Rake:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 3
Rip:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 4
Shred:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 5
Fury:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 6
Bite:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 7
Faerie:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 8
Savage:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 9
Swipe:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 10
Berserk:=linewithjustskill(feralbinds)

ManglePic:=LoadPicture("feral\" A_ScreenHeight "\mangle.png")
FuryPic:=LoadPicture("feral\" A_ScreenHeight "\fury.png")
RakePic:=LoadPicture("feral\" A_ScreenHeight "\rake.png")
RipPic:=LoadPicture("feral\" A_ScreenHeight "\rip.png")
SavagePic:=LoadPicture("feral\" A_ScreenHeight "\savage.png")
ShredPic:=LoadPicture("feral\" A_ScreenHeight "\shred.png")
BerserkPic:=LoadPicture("feral\" A_ScreenHeight "\berserk.png")
BitePic:=LoadPicture("feral\" A_ScreenHeight "\bite.png")
FaeriePic:=LoadPicture("feral\" A_ScreenHeight "\faerie.png")
FaeriePic:=LoadPicture("feral\" A_ScreenHeight "\faerie.png")
AoeFuryPic:=LoadPicture("feral\" A_ScreenHeight "\aoefury.png")
AoeSavagePic:=LoadPicture("feral\" A_ScreenHeight "\aoesavage.png")

~$*XButton2::
    Loop,
    {
        GetKeyState, var, XButton2, P
        If var = U
            Break

        ImageSearch, x, y, aoesavageX1, aoesavageY1, aoesavageX2, aoesavageY2, *50 HBITMAP:*%AoeSavagePic%
        while(ErrorLevel=0)
        {
            GetKeyState, var, XButton2, P
            If var = U
                Break
            ImageSearch, x, y, mangleX1, mangleY1, mangleX2, mangleY2,*50, HBITMAP:*%ManglePic%
            if ErrorLevel = 0
                Send, %Mangle%
            ImageSearch, x, y, rakeX1, rakeY1, rakeX2, rakeY2,*50, HBITMAP:*%RakePic%
            if ErrorLevel = 0
                Send, %Rake%
            ImageSearch, x, y, savageX1, savageY1, savageX2, savageY2,*50, HBITMAP:*%SavagePic%
            if ErrorLevel = 0
                Send, %Savage%
            ImageSearch, x, y, aoesavageX1, aoesavageY1, aoesavageX2, aoesavageY2, *50 HBITMAP:*%AoeSavagePic%
        }

        ImageSearch, x, y, aoefuryX1, aoefuryY1, aoefuryX2, aoefuryY2, *50 HBITMAP:*%AoeFuryPic%
        if (ErrorLevel = 0)
        {
            while(ErrorLevel=0)
            {
                GetKeyState, var, XButton2, P
                If var = U
                    Break
                Send, %Swipe%
                Sleep, 100
                Send, %Fury%
                Sleep, 100
                ImageSearch, x, y, aoefuryX1, aoefuryY1, aoefuryX2, aoefuryY2, *50 HBITMAP:*%AoeFuryPic%
            }
        }
        Send, %Swipe%
    }

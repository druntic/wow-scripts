Menu, Tray, Icon, feral\shred.ico
SetBatchLines, -1
DetectHiddenWindows, On
SetTitleMatchMode, 2
DllCall("dwmapi\DwmEnableComposition", "uint", 0)
#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\setting\supportedResolutions.ahk
#Include, .\setting\closeCurrentScripts.ahk
#Include, .\feral\getFeralCoordinates.ahk
Run, feral\feral aoe.ahk

FileReadLine, feralbinds, feral\binds.txt, 1
Mangle:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 2
Rake:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 3
Rip:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 4
Shred:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 5
Fury:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 6
Bite:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 7
Faerie:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 8
Savage:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 9
Swipe:=linewithjustskill(feralbinds)
FileReadLine, feralbinds, feral\binds.txt, 10
Berserk:=linewithjustskill(feralbinds)

ManglePic:=LoadPicture("feral\" A_ScreenHeight "\mangle.png")
FuryPic:=LoadPicture("feral\" A_ScreenHeight "\fury.png")
RakePic:=LoadPicture("feral\" A_ScreenHeight "\rake.png")
RipPic:=LoadPicture("feral\" A_ScreenHeight "\rip.png")
SavagePic:=LoadPicture("feral\" A_ScreenHeight "\savage.png")
ShredPic:=LoadPicture("feral\" A_ScreenHeight "\shred.png")
BerserkPic:=LoadPicture("feral\" A_ScreenHeight "\berserk.png")
BitePic:=LoadPicture("feral\" A_ScreenHeight "\bite.png")
FaeriePic:=LoadPicture("feral\" A_ScreenHeight "\faerie.png")

Loop,
{
    ImageSearch, x, y, mangleX1, mangleY1, mangleX2, mangleY2,*50, HBITMAP:*%ManglePic%
    if ErrorLevel = 0
        Send, %Mangle%
    ImageSearch, x, y, furyX1, furyY1, furyX2, furyY2,*50, HBITMAP:*%FuryPic%
    if ErrorLevel = 0
        Send, %Fury%
    ImageSearch, x, y, rakeX1, rakeY1, rakeX2, rakeY2,*50, HBITMAP:*%RakePic%
    if ErrorLevel = 0
        Send, %Rake%
    ImageSearch, x, y, ripX1, ripY1, ripX2, ripY2,*50, HBITMAP:*%RipPic%
    if ErrorLevel = 0
        Send, %Rip%
    ImageSearch, x, y, savageX1, savageY1, savageX2, savageY2,*50, HBITMAP:*%SavagePic%
    if ErrorLevel = 0
        Send, %Savage%
    ImageSearch, x, y, shredX1, shredY1, shredX2, shredY2,*50, HBITMAP:*%ShredPic%
    if ErrorLevel = 0
        Send, %Shred%
    ImageSearch, x, y, berserkX1, berserkY1, berserkX2, berserkY2,*50, HBITMAP:*%BerserkPic%
    if ErrorLevel = 0
        Send, %Berserk%
    ImageSearch, x, y, biteX1, biteY1, biteX2, biteY2,*50, HBITMAP:*%BitePic%
    if ErrorLevel = 0
        Send, %Bite%
    ImageSearch, x, y, faerieX1, faerieY1, faerieX2, faerieY2,*50, HBITMAP:*%FaeriePic%
    if ErrorLevel = 0
        Send, %Faerie%
}

#Include, .\setting\pause.ahk
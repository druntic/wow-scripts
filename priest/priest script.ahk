Menu, Tray, Icon, priest\priest.ico
SetBatchLines, -1
DetectHiddenWindows, On
SetTitleMatchMode, 2
DllCall("dwmapi\DwmEnableComposition", "uint", 0)
#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\setting\supportedResolutions.ahk
#Include, .\setting\closeCurrentScripts.ahk
#Include, .\priest\getPriestCoordinates.ahk

FileReadLine, priestBinds, priest\binds.txt, 1
VampiricTouch:=linewithjustskill(priestBinds)
FileReadLine, priestBinds, priest\binds.txt, 2
DevouringPlague:=linewithjustskill(priestBinds)
FileReadLine, priestBinds, priest\binds.txt, 3
MindBlast:=linewithjustskill(priestBinds)
FileReadLine, priestBinds, priest\binds.txt, 4
ShadowWordPain:=linewithjustskill(priestBinds)
FileReadLine, priestBinds, priest\binds.txt, 5
MindFlay:=linewithjustskill(priestBinds)

CastingPic:=LoadPicture("priest\" A_ScreenHeight "\casting.png")
DevouringPlaguePic:=LoadPicture("priest\" A_ScreenHeight "\devouringPlague.png")
MindBlastPic:=LoadPicture("priest\" A_ScreenHeight "\mindBlast.png")
ShadowWordPainPic:=LoadPicture("priest\" A_ScreenHeight "\shadowWordPain.png")
VampiricTouchPic:=LoadPicture("priest\" A_ScreenHeight "\vampiricTouch.png")

Loop,
{
    vampiric = 0
    devouring = 0
    mBlast = 0
    swPain = 0
    casting = 0
    ImageSearch, x, y, vampiricTouchX1, vampiricTouchY1, vampiricTouchX2, vampiricTouchY2, *50 HBITMAP:*%VampiricTouchPic%
    if ErrorLevel = 0
        vampiric = 1
    ImageSearch, x, y, devouringPlagueX1, devouringPlagueY1, devouringPlagueX2, devouringPlagueY2, *50 HBITMAP:*%DevouringPlaguePic%
    if ErrorLevel = 0
        devouring = 1
    ImageSearch, x, y, mindBlastX1, mindBlastY1, mindBlastX2, mindBlastY2, *50 HBITMAP:*%MindBlastPic%
    if ErrorLevel = 0
        mBlast = 1
    ImageSearch, x, y, shadowWordPainX1, shadowWordPainY1, shadowWordPainX2, shadowWordPainY2, *50 HBITMAP:*%ShadowWordPainPic%
    if ErrorLevel = 0
        swPain = 1
    ImageSearch, x, y, castingX1, castingY1, castingX2, castingY2, *50 HBITMAP:*%CastingPic%
    if ErrorLevel = 0
        casting = 1
    if(vampiric && casting!) 
        Send, %VampiricTouch%
    else if(devouring) 
        Send, %DevouringPlague%
    else if(mBlast)
        Send, %MindBlast%
    else if(swPain)
        Send, %ShadowWordPain%
    else 
        Send, %MindFlay%

}


#Include, .\setting\pause.ahk
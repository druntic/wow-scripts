Menu, Tray, Icon, dk pics\frost.ico
SetBatchLines, -1
DetectHiddenWindows, On
SetTitleMatchMode, 2
DllCall("dwmapi\DwmEnableComposition", "uint", 0)
#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\setting\getCoordinates.ahk
#Include, .\setting\supportedResolutions.ahk
#Include, .\setting\closeCurrentScripts.ahk
#Include, .\dk pics\getFdkCoordinates.ahk

FileReadLine, frostbinds, dk pics\binds.txt, 1
Bloodstrike:=linewithjustskill(frostbinds)
FileReadLine, frostbinds, dk pics\binds.txt, 10
Obliterate:=linewithjustskill(frostbinds)
FileReadLine, frostbinds, dk pics\binds.txt, 11
FrostStrike:=linewithjustskill(frostbinds)
FileReadLine, frostbinds, dk pics\binds.txt, 4
Icy:=linewithjustskill(frostbinds)
FileReadLine, frostbinds, dk pics\binds.txt, 5
Plague:=linewithjustskill(frostbinds)
FileReadLine, frostbinds, dk pics\binds.txt, 6
Pestilence:=linewithjustskill(frostbinds)
FileReadLine, frostbinds, dk pics\binds.txt, 12
Howling:=linewithjustskill(frostbinds)
FileReadLine, unholybinds, dk pics\binds.txt, 8
Horn:=linewithjustskill(unholybinds)

BloodstrikePic:=LoadPicture("dk pics\" A_ScreenHeight "\unbloodst.png")
ScourgePic:=LoadPicture("dk pics\" A_ScreenHeight "\scourge.png")
IcyPic:=LoadPicture("dk pics\" A_ScreenHeight "\icy.png")
PlaguePic:=LoadPicture("dk pics\" A_ScreenHeight "\plague.png")
ShieldPic:=LoadPicture("dk pics\" A_ScreenHeight "\shield.png")
PestilencePic:=LoadPicture("dk pics\" A_ScreenHeight "\pestilence.png")
HornPic:=LoadPicture("dk pics\" A_ScreenHeight "\horn.png")
DeathcoilPic:=LoadPicture("dk pics\" A_ScreenHeight "\deathcoil.png")
GargoylePic:=LoadPicture("dk pics\" A_ScreenHeight "\gargoyle.png")
ObliteratePic:=LoadPicture("dk pics\" A_ScreenHeight "\obliterate.png")
HowlingPic:=LoadPicture("dk pics\" A_ScreenHeight "\howlingBlast.png")
FrostStrikePic:=LoadPicture("dk pics\" A_ScreenHeight "\frostStrike.png")
OnePic:=LoadPicture("dk pics\" A_ScreenHeight "\one.png")
ZeroPic:=LoadPicture("dk pics\" A_ScreenHeight "\zero.png")

Loop,
{
    ImageSearch, x, y, oneX1, oneY1, oneX2, oneY2,*50, HBITMAP:*%OnePic%
    if ErrorLevel = 0
    {
        Send, %Pestilence%
    }
    else{
        ImageSearch, x, y, zeroX1, zeroY1, zeroX2, zeroY2,*50, HBITMAP:*%ZeroPic%
        if ErrorLevel = 0
        {
            Send, %Pestilence%
        }
        else
        {
            ImageSearch, x, y, unbloodstX1, unbloodstY1, unbloodstX2, unbloodstY2,*50, HBITMAP:*%BloodstrikePic%
            if ErrorLevel = 0
                Send, %Bloodstrike%
            ImageSearch, x, y, obliterateX1, obliterateY1, obliterateX2, obliterateY2,*50, HBITMAP:*%ObliteratePic%
            if ErrorLevel = 0
                Send, %Obliterate%
            ImageSearch, x, y, icyX1, icyY1, icyX2, icyY2,*50, HBITMAP:*%IcyPic%
            if ErrorLevel = 0
                Send, %Icy%
            ImageSearch, x, y, plagueX1, plagueY1, plagueX2, plagueY2,*50, HBITMAP:*%PlaguePic%
            if ErrorLevel = 0
                Send, %Plague%
            ImageSearch, x, y, frostStrikeX1, frostStrikeY1, frostStrikeX2, frostStrikeY2,*50, HBITMAP:*%FrostStrikePic%
            if ErrorLevel = 0
                Send, %FrostStrike%
            ImageSearch, x, y, pestilenceX1, pestilenceY1, pestilenceX2, pestilenceY2,*50, HBITMAP:*%PestilencePic%
            if ErrorLevel = 0
                Send, %Pestilence%
            ImageSearch, x, y, howlingBlastX1, howlingBlastY1, howlingBlastX2, howlingBlastY2,*50, HBITMAP:*%HowlingPic%
            if ErrorLevel = 0
                Send, %Howling%
        }
    } 
}
#Include, .\setting\pause.ahk


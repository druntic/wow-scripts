Menu, Tray, Icon, dk pics\scourge.ico
SetBatchLines, -1
DetectHiddenWindows, On
SetTitleMatchMode, 2
DllCall("dwmapi\DwmEnableComposition", "uint", 0)
#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\setting\supportedResolutions.ahk
#Include, .\setting\closeCurrentScripts.ahk
#Include, .\dk pics\getUdkCoordinates.ahk

FileReadLine, unholybinds, dk pics\binds.txt, 1
Bloodstrike:=linewithjustskill(unholybinds)
FileReadLine, unholybinds, dk pics\binds.txt, 2
Scourge:=linewithjustskill(unholybinds)
FileReadLine, unholybinds, dk pics\binds.txt, 3
Deathcoil:=linewithjustskill(unholybinds)
FileReadLine, unholybinds, dk pics\binds.txt, 4
Icy:=linewithjustskill(unholybinds)
FileReadLine, unholybinds, dk pics\binds.txt, 5
Plague:=linewithjustskill(unholybinds)
FileReadLine, unholybinds, dk pics\binds.txt, 6
Pestilence:=linewithjustskill(unholybinds)
FileReadLine, unholybinds, dk pics\binds.txt, 7
Shield:=linewithjustskill(unholybinds)
FileReadLine, unholybinds, dk pics\binds.txt, 8
Horn:=linewithjustskill(unholybinds)
FileReadLine, unholybinds, dk pics\binds.txt, 9
Gargoyle:=linewithjustskill(unholybinds)

BloodstrikePic:=LoadPicture("dk pics\" A_ScreenHeight "\unbloodst.png")
ScourgePic:=LoadPicture("dk pics\" A_ScreenHeight "\scourge.png")
IcyPic:=LoadPicture("dk pics\" A_ScreenHeight "\icy.png")
PlaguePic:=LoadPicture("dk pics\" A_ScreenHeight "\plague.png")
ShieldPic:=LoadPicture("dk pics\" A_ScreenHeight "\shield.png")
PestilencePic:=LoadPicture("dk pics\" A_ScreenHeight "\pestilence.png")
HornPic:=LoadPicture("dk pics\" A_ScreenHeight "\horn.png")
DeathcoilPic:=LoadPicture("dk pics\" A_ScreenHeight "\deathcoil.png")
Loop,
{
    ImageSearch, x, y, unbloodstX1 ,unbloodstY1 ,unbloodstX2 ,unbloodstY2,*50, HBITMAP:*%BloodstrikePic%
    if ErrorLevel = 0
        Send, %Bloodstrike%
    ImageSearch, x, y, scourgeX1 ,scourgeY1 ,scourgeX2 ,scourgeY2,*50, HBITMAP:*%ScourgePic%
    if ErrorLevel = 0
        Send, %Scourge%
    ImageSearch, x, y, icyX1 ,icyY1 ,icyX2 ,icyY2,*50, HBITMAP:*%IcyPic%
    if ErrorLevel = 0
        Send, %Icy%
    ImageSearch, x, y, plagueX1 ,plagueY1 ,plagueX2 ,plagueY2,*50, HBITMAP:*%PlaguePic%
    if ErrorLevel = 0
        Send, %Plague%
    ImageSearch, x, y, shieldX1 ,shieldY1 ,shieldX2 ,shieldY2,*50, HBITMAP:*%ShieldPic%
    if ErrorLevel = 0
        Send, %Shield%
    ImageSearch, x, y, pestilenceX1 ,pestilenceY1 ,pestilenceX2 ,pestilenceY2,*50, HBITMAP:*%PestilencePic%
    if ErrorLevel = 0
        Send, %Pestilence%
    ImageSearch, x, y, deathcoilX1 ,deathcoilY1 ,deathcoilX2 ,deathcoilY2,*50, HBITMAP:*%DeathcoilPic%
    if ErrorLevel = 0
        Send, %Deathcoil%
}
#Include, .\setting\pause.ahk
Menu, Tray, Icon, dk pics\blood.ico
SetBatchLines, -1
DetectHiddenWindows, On
SetTitleMatchMode, 2
DllCall("dwmapi\DwmEnableComposition", "uint", 0)
#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\dk pics\getBdkCoordinates.ahk
#Include, .\setting\supportedResolutions.ahk
#Include, .\setting\closeCurrentScripts.ahk
;load binds
FileReadLine, bloodbinds, dk pics\binds.txt, 14
Heartstrike:=linewithjustskill(bloodbinds)
FileReadLine, bloodbinds, dk pics\binds.txt, 13
DeathStrike:=linewithjustskill(bloodbinds)
FileReadLine, bloodbinds, dk pics\binds.txt, 3
Deathcoil:=linewithjustskill(bloodbinds)
FileReadLine, bloodbinds, dk pics\binds.txt, 4
Icy:=linewithjustskill(bloodbinds)
FileReadLine, bloodbinds, dk pics\binds.txt, 5
Plague:=linewithjustskill(bloodbinds)
FileReadLine, bloodbinds, dk pics\binds.txt, 6
Pestilence:=linewithjustskill(bloodbinds)
FileReadLine, bloodbinds, dk pics\binds.txt, 12
DRW:=linewithjustskill(bloodbinds)
FileReadLine, unholybinds, dk pics\binds.txt, 15
Horn:=linewithjustskill(unholybinds)

;load pictures
OnePic:=LoadPicture("dk pics\" A_ScreenHeight "\one.png")
ZeroPic:=LoadPicture("dk pics\" A_ScreenHeight "\zero.png")
BloodrunePic:=LoadPicture("dk pics\" A_ScreenHeight "\bloodrune.png")
HeartstrikePic:=LoadPicture("dk pics\" A_ScreenHeight "\heartStrike.png")
DeathStrikePic:=LoadPicture("dk pics\" A_ScreenHeight "\deathStrike.png")
IcyPic:=LoadPicture("dk pics\" A_ScreenHeight "\icy.png")
PlaguePic:=LoadPicture("dk pics\" A_ScreenHeight "\plague.png")
DeathcoilPic:=LoadPicture("dk pics\" A_ScreenHeight "\deathcoil.png")
DRWReadyPic:=LoadPicture("dk pics\" A_ScreenHeight "\dancingRuneWeaponRDY.png")

Loop,
{

    ImageSearch, x, y, oneX1, oneY1, oneX2, oneY2,*50, HBITMAP:*%OnePic%
    if ErrorLevel = 0
    {
        ImageSearch, x, y, bloodruneX1,bloodruneY1, bloodruneX2,bloodruneY2,*50, HBITMAP:*%BloodrunePic%
        if ErrorLevel = 0
            Send, %Pestilence%
    }
    else{
        ImageSearch, x, y, zeroX1, zeroY1, zeroX2, zeroY2,*50, HBITMAP:*%ZeroPic%
        if ErrorLevel = 0
        {
            ImageSearch, x, y, bloodruneX1,bloodruneY1, bloodruneX2,bloodruneY2,*50, HBITMAP:*%BloodrunePic%
            if ErrorLevel = 0
                Send, %Pestilence%
        }
        else
        {
            ImageSearch, x, y, heartStrikeX1, heartStrikeY1, heartStrikeX2, heartStrikeY2,*50, HBITMAP:*%HeartstrikePic%
            if ErrorLevel = 0
                Send, %Heartstrike%
            ImageSearch, x, y, deathStrikeX1, deathStrikeY1, deathStrikeX2, deathStrikeY2,*50, HBITMAP:*%DeathStrikePic%
            if ErrorLevel = 0
                Send, %DeathStrike%
            ImageSearch, x, y, icyX1, icyY1, icyX2, icyY2,*50, HBITMAP:*%IcyPic%
            if ErrorLevel = 0
                Send, %Icy%
            ImageSearch, x, y, plagueX1, plagueY1, plagueX2, plagueY2,*50, HBITMAP:*%PlaguePic%
            if ErrorLevel = 0
                Send, %Plague%
            ImageSearch, x, y, deathcoilX1 ,deathcoilY1 ,deathcoilX2 ,deathcoilY2,*50, HBITMAP:*%DeathcoilPic%
            if ErrorLevel = 0
            {
                ;MsgBox, %dancingRuneWeaponRDYX1%, %dancingRuneWeaponRDYY1%, %dancingRuneWeaponRDYX2%, %dancingRuneWeaponRDYY2%
                ImageSearch, x, y, dancingRuneWeaponRDYX1, dancingRuneWeaponRDYY1, dancingRuneWeaponRDYX2, dancingRuneWeaponRDYY2 ,*50, HBITMAP:*%DRWReadyPic%
                if ErrorLevel = 0
                    Send, %DRW%
                else
                {
                    Send, %Deathcoil%
                }

            }

        }
    }

}

#Include, .\setting\pause.ahk
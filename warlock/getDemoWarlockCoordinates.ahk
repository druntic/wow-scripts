FilePath := A_ScriptDir "\demoCoordinates.txt"
; Check if the file exists and read coordinates if it does
if FileExist(FilePath) {
    FileRead, FileContent, %FilePath%
    StringSplit, lines, FileContent, `r

    pictures := ["castingImmolate", "castingIncinerate", "castingShadowbolt", "corruption", "curse", "decimation", "immolate", "lifeTap", "moltenCore", "moltenCore1Stack", "shadowMastery"]
    if(lines0 - 1 != pictures.MaxIndex()) {
        MsgBox, Some box coordinates are missing!
        MsgBox, % (lines0 - 1) ", " pictures.Length()
        ExitApp
    }
    for key, value in pictures {
        line := Trim(lines%A_Index%)
        StringSplit, coords, line, `, 
        if (coords0 != 4) {
            MsgBox, Invalid coordinates!
            ExitApp
        }
        %value%X1 := coords1
        %value%Y1 := coords2
        %value%X2 := coords3
        %value%Y2 := coords4
    }
}

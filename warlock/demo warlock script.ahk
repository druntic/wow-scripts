Menu, Tray, Icon, warlock\demo.ico
SetBatchLines, -1
DetectHiddenWindows, On
SetTitleMatchMode, 2
DllCall("dwmapi\DwmEnableComposition", "uint", 0)
#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\warlock\getDemoWarlockCoordinates.ahk
#Include, .\setting\supportedResolutions.ahk
#Include, .\setting\closeCurrentScripts.ahk

FileReadLine, bloodbinds, warlock\binds.txt, 1
Corruption:=linewithjustskill(bloodbinds)
FileReadLine, bloodbinds, warlock\binds.txt, 2
Curse:=linewithjustskill(bloodbinds)
FileReadLine, bloodbinds, warlock\binds.txt, 3
Soulfire:=linewithjustskill(bloodbinds)
FileReadLine, bloodbinds, warlock\binds.txt, 4
Immolate:=linewithjustskill(bloodbinds)
FileReadLine, bloodbinds, warlock\binds.txt, 5
Incirenate:=linewithjustskill(bloodbinds)
FileReadLine, bloodbinds, warlock\binds.txt, 6
Shadowbolt:=linewithjustskill(bloodbinds)
FileReadLine, bloodbinds, warlock\binds.txt, 7
Lifetap:=linewithjustskill(bloodbinds)

LifetapPic:=LoadPicture("warlock\" A_ScreenHeight "\lifeTap.png")
CursePic:=LoadPicture("warlock\" A_ScreenHeight "\curse.png")
ImmolatePic:=LoadPicture("warlock\" A_ScreenHeight "\immolate.png")
CastingImmolatePic:=LoadPicture("warlock\" A_ScreenHeight "\castingImmolate.png")
CastingIncineratePic:=LoadPicture("warlock\" A_ScreenHeight "\castingIncinerate.png")
CastingShadowboltPic:=LoadPicture("warlock\" A_ScreenHeight "\castingShadowbolt.png")
CorruptionPic:=LoadPicture("warlock\" A_ScreenHeight "\corruption.png")
ShadowMasteryPic:=LoadPicture("warlock\" A_ScreenHeight "\shadowMastery.png")
MoltenCorePic:=LoadPicture("warlock\" A_ScreenHeight "\moltenCore.png")
MoltenCore1stackPic:=LoadPicture("warlock\" A_ScreenHeight "\moltenCore1Stack.png")
DecimationPic:=LoadPicture("warlock\" A_ScreenHeight "\decimation.png")

Loop,
{
  CastingImmolate = 0
  CastingIncinerate = 0
  CastingShadowbolt = 0
  LifetapDebuff = 0
  CurseDebuff = 0
  ImmolateDebuff = 0
  CorruptionDebuff = 0
  ShadowboltDebuff = 0
  MoltenCorebuff = 0
  DecimationBuff = 0
  MoltenCore1Stack = 0

  ImageSearch, x, y, lifeTapX1, lifeTapY1, lifeTapX2, lifeTapY2, *50 HBITMAP:*%LifetapPic%
  if ErrorLevel = 0
    LifetapDebuff = 1
  ImageSearch, x, y, curseX1, curseY1, curseX2, curseY2, *50 HBITMAP:*%CursePic%
  if ErrorLevel = 0
    CurseDebuff = 1
  ImageSearch, x, y, immolateX1, immolateY1, immolateX2, immolateY2, *50 HBITMAP:*%ImmolatePic%
  if (ErrorLevel = 0)
    ImmolateDebuff = 1
  ImageSearch, x, y, castingImmolateX1, castingImmolateY1, castingImmolateX2, castingImmolateY2, *50 HBITMAP:*%CastingImmolatePic%
  if ErrorLevel = 0
    CastingImmolate = 1
  ImageSearch, x, y, castingShadowboltX1, castingShadowboltY1, castingShadowboltX2, castingShadowboltY2, *50 HBITMAP:*%CastingShadowboltPic%
  if ErrorLevel = 0
    CastingShadowbolt = 1
  ImageSearch, x, y, castingIncinerateX1, castingIncinerateY1, castingIncinerateX2, castingIncinerateY2, *50 HBITMAP:*%CastingIncineratePic%
  if ErrorLevel = 0
    CastingIncinerate = 1
  ImageSearch, x, y, corruptionX1, corruptionY1, corruptionX2, corruptionY2, *50 HBITMAP:*%CorruptionPic%
  if ErrorLevel = 0
    CorruptionDebuff = 1
  ImageSearch, x, y, shadowMasteryX1, shadowMasteryY1, shadowMasteryX2, shadowMasteryY2, *50 HBITMAP:*%ShadowMasteryPic%
  if ErrorLevel = 0
    ShadowboltDebuff = 1
  ImageSearch, x, y, moltenCoreX1, moltenCoreY1, moltenCoreX2, moltenCoreY2, *50 HBITMAP:*%MoltenCorePic%
  if ErrorLevel = 0
    MoltenCorebuff = 1
  ImageSearch, x, y, decimationX1, decimationY1, decimationX2, decimationY2, *50 HBITMAP:*%DecimationPic%
  if ErrorLevel = 0
    DecimationBuff = 1
  ImageSearch, x, y, moltenCore1StackX1, moltenCore1StackY1, moltenCore1StackX2, moltenCore1StackY2, *50 HBITMAP:*%MoltenCore1stackPic%
  if ErrorLevel = 0
    MoltenCore1Stack = 1

  if (LifetapDebuff)
    Send, %Lifetap%
  else if(CorruptionDebuff) 
    Send, %Corruption%
  else if(ImmolateDebuff && CastingImmolate) {
    if(MoltenCorebuff)
      Send, %Incirenate%
    else if (CurseDebuff)
      Send, %Curse%
    else if (ShadowboltDebuff)
      Send, %Shadowbolt%
    else if (DecimationBuff)
      Send, %Soulfire%
    else
      Send, %Shadowbolt%
  }
  else if(MoltenCorebuff && CastingIncinerate && !DecimationBuff) {
    if (MoltenCore1Stack == 1) {
      if(CorruptionDebuff) 
        Send, %Corruption%
      else if(ImmolateDebuff)
        Send, %Immolate%
      else if (CurseDebuff)
        Send, %Curse%
      else if (ShadowboltDebuff)
        Send, %Shadowbolt%
      else if (DecimationBuff)
        Send, %Soulfire%
      else
        Send, %Shadowbolt%
    }
    else
      Send, %Incirenate%
  }
  else if (ShadowboltDebuff && !CastingShadowbolt)
    Send, %Shadowbolt%
  else if(ImmolateDebuff)
    Send, %Immolate%
  else if (!DecimationBuff && MoltenCorebuff)
    Send, %Incirenate%
  else if (DecimationBuff)
    Send, %Soulfire%
  else if (CurseDebuff)
    Send, %Curse%
  else
    Send, %Shadowbolt%

}

#Include, .\setting\pause.ahk
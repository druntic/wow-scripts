Menu, Tray, Icon, moonkin\moonkin.ico
SetBatchLines, -1
DetectHiddenWindows, On
SetTitleMatchMode, 2
DllCall("dwmapi\DwmEnableComposition", "uint", 0)
#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\setting\supportedResolutions.ahk
#Include, .\setting\closeCurrentScripts.ahk
#Include, .\moonkin\getMoonkinCoordinates.ahk
FileReadLine, boomybinds, moonkin\binds.txt, 1
Wrath:=linewithjustskill(boomybinds)
FileReadLine, boomybinds, moonkin\binds.txt, 2
Starfire:=linewithjustskill(boomybinds)
FileReadLine, boomybinds, moonkin\binds.txt, 3
Swarm:=linewithjustskill(boomybinds)
FileReadLine, boomybinds, moonkin\binds.txt, 4
Moonfire:=linewithjustskill(boomybinds)


EclipsePic:=LoadPicture("moonkin\" A_ScreenHeight "\eclipse.png")
InsectSwarmPic:=LoadPicture("moonkin\" A_ScreenHeight "\insectSwarm.png")
MoonfirePic:=LoadPicture("moonkin\" A_ScreenHeight "\moonfire.png")
SolarPic:=LoadPicture("moonkin\" A_ScreenHeight "\solar.png")
Loop,
{
  ;todo: improve rotation
  ImageSearch, x, y, moonfireX1, moonfireY1, moonfireX2, moonfireY2, *50 HBITMAP:*%MoonfirePic%
  if (ErrorLevel = 0)
  {
    Send, %Moonfire%
    Sleep, 100
  }
  ImageSearch, x, y, insectSwarmX1, insectSwarmY1, insectSwarmX2, insectSwarmY2, *50 HBITMAP:*%InsectSwarmPic%
  if (ErrorLevel = 0)
  {
    Send, %Swarm%
    Sleep, 100
  }
  ImageSearch, x, y, eclipseX1, eclipseY1, eclipseX2, eclipseY2, *50 HBITMAP:*%EclipsePic%
  if (ErrorLevel = 0)
  {
    ImageSearch, x, y, solarX1, solarY1, solarX2, solarY2, *50 HBITMAP:*%SolarPic%
    while(ErrorLevel = 1)
    {
      ImageSearch, x, y, moonfireX1, moonfireY1, moonfireX2, moonfireY2, *50 HBITMAP:*%MoonfirePic%
      if (ErrorLevel = 0)
      {
        Send, %Moonfire%
        Sleep, 100
      }
      ImageSearch, x, y, insectSwarmX1, insectSwarmY1, insectSwarmX2, insectSwarmY2, *50 HBITMAP:*%InsectSwarmPic%
      if (ErrorLevel = 0)
      {
        Send, %Swarm%
        Sleep, 100
      }
      Send,%Starfire%
      Sleep,100
      ImageSearch, x, y, solarX1, solarY1, solarX2, solarY2, *50 HBITMAP:*%SolarPic%
    }
  }
  Send, %Wrath%
  Sleep,100
}

#Include, .\setting\pause.ahk
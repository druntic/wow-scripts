Menu, Tray, Icon, rogue\rogue.ico
SetBatchLines, -1
DetectHiddenWindows, On
SetTitleMatchMode, 2
DllCall("dwmapi\DwmEnableComposition", "uint", 0)
#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\rogue\getRogueCoordinates.ahk
#Include, .\setting\supportedResolutions.ahk
#Include, .\setting\closeCurrentScripts.ahk


FileReadLine, roguebinds, rogue\binds.txt, 1
Sinister:=linewithjustskill(roguebinds)
FileReadLine, roguebinds, rogue\binds.txt, 2
Rupture:=linewithjustskill(roguebinds)
FileReadLine, roguebinds, rogue\binds.txt, 3
Slice:=linewithjustskill(roguebinds)
FileReadLine, roguebinds, rogue\binds.txt, 4
Eviscerate:=linewithjustskill(roguebinds)
FileReadLine, roguebinds, rogue\binds.txt, 5
FanOfKnives:=linewithjustskill(roguebinds)

OnePic:=LoadPicture("rogue\" A_ScreenHeight "\1.png")
TwoPic:=LoadPicture("rogue\" A_ScreenHeight "\2.png")
ThreePic:=LoadPicture("rogue\" A_ScreenHeight "\3.png")
FourPic:=LoadPicture("rogue\" A_ScreenHeight "\4.png")
FivePic:=LoadPicture("rogue\" A_ScreenHeight "\5.png")
SD1Pic:=LoadPicture("rogue\" A_ScreenHeight "\SD1.png")
SD2Pic:=LoadPicture("rogue\" A_ScreenHeight "\SD2.png")
SDPic:=LoadPicture("rogue\" A_ScreenHeight "\SD.png")

Loop, {
    cp:=0
    slicetimer:=0
    rupturetimer:=0
    ImageSearch, x, y, 1X1, 1Y1, 1X2, 1Y2, *50 HBITMAP:*%OnePic%
    if ErrorLevel = 0
        cp:=1
    ImageSearch, x, y, 2X1, 2Y1, 2X2, 2Y2, *50 HBITMAP:*%TwoPic%
    if ErrorLevel = 0
        cp:=2
    ImageSearch, x, y, 3X1, 3Y1, 3X2, 3Y2, *50 HBITMAP:*%ThreePic%
    if ErrorLevel = 0
        cp:=3
    ImageSearch, x, y, 4X1, 4Y1, 4X2, 4Y2, *50 HBITMAP:*%FourPic%
    if ErrorLevel = 0
        cp:=4
    ImageSearch, x, y, 5X1, 5Y1, 5X2, 5Y2, *50 HBITMAP:*%FivePic%
    if ErrorLevel = 0
        cp=5
    ImageSearch, x, y, SDX1, SDY1, SDX2, SDY2, *50 HBITMAP:*%SDPic%
    if ErrorLevel = 1
        slicetimer:=1
    ImageSearch, x, y, SD2X1, SD2Y1, SD2X2, SD2Y2, *50 HBITMAP:*%SD2Pic%
    if ErrorLevel = 0
        slicetimer = 1
    ImageSearch, x, y, SD1X1, SD1Y1, SD1X2, SD1Y2, *50 HBITMAP:*%SD1Pic%
    if ErrorLevel = 0
        slicetimer = 1
    

    GetKeyState, var, XButton2, P
    If var = U
    {
        if (cp>0 and slicetimer=1)
            Send, %Slice%
        else if(cp>=4)
            Send, %Eviscerate%
        else
            Send, %Sinister%

    }

    else{
        if (cp>0 and slicetimer=1)
            Send, %Slice%
        else if(cp=0 and slicetimer=1)
            Send, %Sinister%
        else
            Send, %FanOfKnives%
    }
}

#Include, .\setting\pause.ahk
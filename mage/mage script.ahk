Menu, Tray, Icon, mage\mage.ico
SetBatchLines, -1
DetectHiddenWindows, On
SetTitleMatchMode, 2
DllCall("dwmapi\DwmEnableComposition", "uint", 0)
#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\mage\getMageCoordinates.ahk
#Include, .\setting\closeCurrentScripts.ahk

FileReadLine, magebinds, mage\binds.txt, 1
Fireball:=linewithjustskill(magebinds)
FileReadLine, magebinds, mage\binds.txt, 2
Livingbomb:=linewithjustskill(magebinds)
FileReadLine, magebinds, mage\binds.txt, 3
Pyro:=linewithjustskill(magebinds)
FileReadLine, magebinds, mage\binds.txt, 4
Scorch:=linewithjustskill(magebinds)

LivingBombPic:=LoadPicture("mage\" A_ScreenHeight "\LivingBomb.png")
PyroPic:=LoadPicture("mage\" A_ScreenHeight "\Pyro.png")
ScorchPic:=LoadPicture("mage\" A_ScreenHeight "\Scorch.png")

Loop,
{
  ImageSearch, x, y, PyroX1, PyroY1, PyroX2, PyroY2, *50 HBITMAP:*%PyroPic%
  if (ErrorLevel = 0){
    Send, %Pyro%
    ImageSearch, x, y, PyroX1, PyroY1, PyroX2, PyroY2, *50 HBITMAP:*%PyroPic%
  }
  else{
    ImageSearch, x, y, ScorchX1, ScorchY1, ScorchX2, ScorchY2, *50 HBITMAP:*%ScorchPic%
    if(ErrorLevel = 0){
      Send, %Scorch%
    }
    ImageSearch, x, y, LivingBombX1, LivingBombY1, LivingBombX2, LivingBombY2, *50 HBITMAP:*%LivingBombPic%
    if (ErrorLevel = 0){
      Send, %Livingbomb%
    }
    else{
      Send, %Fireball%
    }
  }

}
#Include, .\setting\pause.ahk
Menu, Tray, Icon, hunter\hunter.ico
SetBatchLines, -1
DetectHiddenWindows, On
SetTitleMatchMode, 2
DllCall("dwmapi\DwmEnableComposition", "uint", 0)
#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\hunter\getHunterCoordinates.ahk
#Include, .\setting\closeCurrentScripts.ahk

FileReadLine, hunterbinds, hunter\binds.txt, 1
Aimed:=linewithjustskill(hunterbinds)
FileReadLine, hunterbinds, hunter\binds.txt, 2
Chimera:=linewithjustskill(hunterbinds)
FileReadLine, hunterbinds, hunter\binds.txt, 3
Mark:=linewithjustskill(hunterbinds)
FileReadLine, hunterbinds, hunter\binds.txt, 4
Steady:=linewithjustskill(hunterbinds)
FileReadLine, hunterbinds, hunter\binds.txt, 5
Serpent:=linewithjustskill(hunterbinds)
FileReadLine, hunterbinds, hunter\binds.txt, 6
Killing:=linewithjustskill(hunterbinds)

aimShotPic:=LoadPicture("hunter\" A_ScreenHeight "\aimShot.png")
chimeraShotPic:=LoadPicture("hunter\" A_ScreenHeight "\chimeraShot.png")
huntersMarkPic:=LoadPicture("hunter\" A_ScreenHeight "\huntersMark.png")
killshotPic:=LoadPicture("hunter\" A_ScreenHeight "\killShot.png")
serpentShotPic:=LoadPicture("hunter\" A_ScreenHeight "\serpentSting.png")

Loop,
{
    serpent1 = 0
    chimera1 = 0
    aimed1 = 0
    killing1 = 0
    huntersMark1 = 0
    ImageSearch, x, y, serpentStingX1, serpentStingY1, serpentStingX2, serpentStingY2, *50 HBITMAP:*%serpentShotPic%
    if ErrorLevel = 0
        serpent1 = 1
    ImageSearch, x, y, chimeraShotX1, chimeraShotY1, chimeraShotX2, chimeraShotY2, *50 HBITMAP:*%chimeraShotPic%
    if ErrorLevel = 0
        chimera1 = 1
    ImageSearch, x, y, aimShotX1, aimShotY1, aimShotX2, aimShotY2, *50 HBITMAP:*%aimShotPic%
    if ErrorLevel = 0
        aimed1 = 1
    ImageSearch, x, y, killShotX1, killShotY1, killShotX2, killShotY2, *50 HBITMAP:*%killshotPic%
    if ErrorLevel = 0
        killing1 = 1
    ImageSearch, x, y, huntersMarkX1, huntersMarkY1, huntersMarkX2, huntersMarkY2, *50 HBITMAP:*%huntersMarkPic%
    if ErrorLevel = 0
        huntersMark1 = 1

    if(serpent1)
        Send, %Serpent%
    else if(killing1)
        Send, %Killing%
    else if(chimera1)
        Send, %Chimera%
    else if(aimed1)
        Send, %Aimed%
    else if(huntersMark1)
        Send, %Mark%
    else
        Send, %Steady%
}

#Include, .\setting\pause.ahk
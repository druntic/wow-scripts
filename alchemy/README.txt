1.Create 6 macros using the macro folder and bind each macro to its corresponding name
eg. numpad1.txt will be bound to the numpad1 key.
2.Replace the character name in numpad6.txt with the next character in the list
eg. SendMail("Dado", ".", "") will send a mail to the character Dado
3.Create a macro with a unique icon on the last character which will do transmute and place is anywhere in your bars. This will be the trigger to stop the script when we are on the final transmute character.
4.Crop a part of the icon created in step 3 and save it as lastChar.png.
5.Open your mailbox and crop the Inbox name as shown in image help1.png. Save the image as inbox.png.
6.Go to your character page and crop the WoW logo. Save the image as wowicon.png.
FilePath := A_ScriptDir "\coordinates.txt"
; Check if the file exists and read coordinates if it does
if FileExist(FilePath) {
    FileRead, FileContent, %FilePath%
    StringSplit, lines, FileContent, `r
    
    pictures := ["hammer", "cons", "crusader", "divineStorm", "exorcism", , "judgement", "sealOfVengence", "sealOfCommand", "sealStack1", "sealStack2", "sealStack3", "sealStack4", "shield"]
    if(lines0 != pictures.MaxIndex()) {
        MsgBox, Some box coordinates are missing!
        MsgBox, % (lines0 - 1) ", " pictures.Length()
        ExitApp
    }
    for key, value in pictures {
        line := Trim(lines%A_Index%)
        StringSplit, coords, line, `, 
        if (coords0 != 4) {
            MsgBox, Invalid coordinates!
            ExitApp
        }
        %value%X1 := coords1
        %value%Y1 := coords2
        %value%X2 := coords3
        %value%Y2 := coords4
    }
}

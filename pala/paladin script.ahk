Menu, Tray, Icon, pala\divine.ico
SetBatchLines, -1
DetectHiddenWindows, On
SetTitleMatchMode, 2
DllCall("dwmapi\DwmEnableComposition", "uint", 0)
#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\setting\getCoordinates.ahk
#Include, .\setting\supportedResolutions.ahk
#Include, .\setting\closeCurrentScripts.ahk
#Include, .\pala\getRetCoordinates.ahk

;load binds
FileReadLine, palabinds, pala\binds.txt, 1
Divinestorm:=linewithjustskill(palabinds)
FileReadLine, palabinds, pala\binds.txt, 2
Crusader:=linewithjustskill(palabinds)
FileReadLine, palabinds, pala\binds.txt, 3
Judgement:=linewithjustskill(palabinds)
FileReadLine, palabinds, pala\binds.txt, 4
Exorcism:=linewithjustskill(palabinds)
FileReadLine, palabinds, pala\binds.txt, 5
Hammer:=linewithjustskill(palabinds)
FileReadLine, palabinds, pala\binds.txt, 6
Cons:=linewithjustskill(palabinds)
FileReadLine, palabinds, pala\binds.txt, 7
Shield:=linewithjustskill(palabinds)
FileReadLine, palabinds, pala\binds.txt, 8
Seal:=linewithjustskill(palabinds)
;load pictures
SealStack4Pic:=LoadPicture("pala\" A_ScreenHeight "\SealStack4.png")
SealStack3Pic:=LoadPicture("pala\" A_ScreenHeight "\SealStack3.png")
SealStack2Pic:=LoadPicture("pala\" A_ScreenHeight "\SealStack2.png")
SealStack1Pic:=LoadPicture("pala\" A_ScreenHeight "\SealStack1.png")
HammerPic:=LoadPicture("pala\" A_ScreenHeight "\hammer.png")
JudgementPic:=LoadPicture("pala\" A_ScreenHeight "\judgement.png")
CrusaderPic:=LoadPicture("pala\" A_ScreenHeight "\crusader.png")
DivinestormPic:=LoadPicture("pala\" A_ScreenHeight "\divineStorm.png")
ConsPic:=LoadPicture("pala\" A_ScreenHeight "\cons.png")
ExorcismPic:=LoadPicture("pala\" A_ScreenHeight "\exorcism.png")
ShieldPic:=LoadPicture("pala\" A_ScreenHeight "\shield.png")
SealOfVengencePic:=LoadPicture("pala\" A_ScreenHeight "\sealOfVengence.png")
SealOfCommandPic:=LoadPicture("pala\" A_ScreenHeight "\sealOfCommand.png")
Loop,
{
    Loop,
    {
        GetKeyState, var, XButton2, P
        If var = U
            Break
        lessThan4 = 0
        ImageSearch, x, y, SealStack4X1, SealStack4Y1, SealStack4X2, SealStack4Y2, *50 HBITMAP:*%SealStack4Pic%
        if ErrorLevel = 0
            lessThan4 = 1
        ImageSearch, x, y, SealStack3X1, SealStack3Y1, SealStack3X2, SealStack3Y2, *50 HBITMAP:*%SealStack3Pic%
        if ErrorLevel = 0
            lessThan4 = 1
        ImageSearch, x, y, SealStack2X1, SealStack2Y1, SealStack2X2, SealStack2Y2, *50 HBITMAP:*%SealStack2Pic%
        if ErrorLevel = 0
            lessThan4 = 1
        ImageSearch, x, y, SealStack1X1, SealStack1Y1, SealStack1X2, SealStack1Y2, *50 HBITMAP:*%SealStack1Pic%
        if ErrorLevel = 0
            lessThan4 = 1
        if lessThan4 = 1
        {
            ImageSearch, x, y, sealOfCommandX1, sealOfCommandY1, sealOfCommandX2, sealOfCommandY2, *50 HBITMAP:*%SealOfCommandPic%
            if ErrorLevel = 0
                Send, %Seal%
        }
        else{
            ImageSearch, x, y, sealOfVengenceX1, sealOfVengenceY1, sealOfVengenceX2, sealOfVengenceY2, *50 HBITMAP:*%SealOfVengencePic%
            if ErrorLevel = 0
                Send, %Seal%
        }
        ImageSearch, x, y, hammerX1, hammerY1, hammerX2, hammerY2, HBITMAP:*%HammerPic%
        if ErrorLevel = 0
            Send, %Hammer%
        ImageSearch, x, y, judgementX1, judgementY1, judgementX2, judgementY2, HBITMAP:*%JudgementPic%
        if ErrorLevel = 0
            Send, %Judgement%
        ImageSearch, x, y, crusaderX1, crusaderY1, crusaderX2, crusaderY2, HBITMAP:*%CrusaderPic%
        if ErrorLevel = 0
            Send, %Crusader%
        ImageSearch, x, y, divineStormX1, divineStormY1, divineStormX2, divineStormY2, HBITMAP:*%DivinestormPic%
        if ErrorLevel = 0
            Send, %Divinestorm%
        ImageSearch, x, y, consX1, consY1, consX2, consY2, HBITMAP:*%ConsPic%
        if ErrorLevel = 0
            Send, %Cons%
        ImageSearch, x, y, exorcismX1, exorcismY1, exorcismX2, exorcismY2, HBITMAP:*%ExorcismPic%
        if ErrorLevel = 0
            Send, %Exorcism%
        ImageSearch, x, y, shieldX1, shieldY1, shieldX2, shieldY2, HBITMAP:*%ShieldPic%
        if ErrorLevel = 0
            Send, %Shield%
    }
    ImageSearch, x, y, sealOfCommandX1, sealOfCommandY1, sealOfCommandX2, sealOfCommandY2, *50 HBITMAP:*%SealOfCommandPic%
    if ErrorLevel = 0
        Send, %Seal%
    ImageSearch, x, y, hammerX1, hammerY1, hammerX2, hammerY2, HBITMAP:*%HammerPic%
    if ErrorLevel = 0
        Send, %Hammer%
    ImageSearch, x, y, judgementX1, judgementY1, judgementX2, judgementY2, HBITMAP:*%JudgementPic%
    if ErrorLevel = 0
        Send, %Judgement%
    ImageSearch, x, y, crusaderX1, crusaderY1, crusaderX2, crusaderY2, HBITMAP:*%CrusaderPic%
    if ErrorLevel = 0
        Send, %Crusader%
    ImageSearch, x, y, divineStormX1, divineStormY1, divineStormX2, divineStormY2, HBITMAP:*%DivinestormPic%
    if ErrorLevel = 0
        Send, %Divinestorm%
    ImageSearch, x, y, consX1, consY1, consX2, consY2, HBITMAP:*%ConsPic%
    if ErrorLevel = 0
        Send, %Cons%
    ImageSearch, x, y, exorcismX1, exorcismY1, exorcismX2, exorcismY2, HBITMAP:*%ExorcismPic%
    if ErrorLevel = 0
        Send, %Exorcism%
    ImageSearch, x, y, shieldX1, shieldY1, shieldX2, shieldY2, HBITMAP:*%ShieldPic%
    if ErrorLevel = 0
        Send, %Shield%

}

#Include, .\setting\pause.ahk
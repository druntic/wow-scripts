Menu, Tray, Icon, warrior\whirlwind.ico
SetBatchLines, -1
DetectHiddenWindows, On
SetTitleMatchMode, 2
DllCall("dwmapi\DwmEnableComposition", "uint", 0)
#SingleInstance Force
#Include, .\setting\help functions.ahk
#Include, .\setting\supportedResolutions.ahk
#Include, .\setting\closeCurrentScripts.ahk
#Include, .\warrior\getWarriorCoordinates.ahk


; load binds
FileReadLine, warrbinds, warrior\binds.txt, 1
Bloodthirst:=linewithjustskill(warrbinds)
FileReadLine, warrbinds, warrior\binds.txt, 2
Execute:=linewithjustskill(warrbinds)
FileReadLine, warrbinds, warrior\binds.txt, 3
Heroic:=linewithjustskill(warrbinds)
FileReadLine, warrbinds, warrior\binds.txt, 4
Slam:=linewithjustskill(warrbinds)
FileReadLine, warrbinds, warrior\binds.txt, 5
Sunder:=linewithjustskill(warrbinds)
FileReadLine, warrbinds, warrior\binds.txt, 6
whirlwind:=linewithjustskill(warrbinds)
; load pictures
BloodthirstPic:=LoadPicture("warrior\" A_ScreenHeight "\bloodThirst.png")
WhirlwindPic:=LoadPicture("warrior\" A_ScreenHeight "\whirlwind.png")
HeroicThrowPic:=LoadPicture("warrior\" A_ScreenHeight "\heroicThrow.png")
SunderPic:=LoadPicture("warrior\" A_ScreenHeight "\sunder.png")
SlamPic:=LoadPicture("warrior\" A_ScreenHeight "\slam.png")
ExecutePic:=LoadPicture("warrior\" A_ScreenHeight "\execute.png")
DemoShoutPic:=LoadPicture("warrior\" A_ScreenHeight "\demoShout.png")



Loop,
{
    Loop,
    {
        GetKeyState, var, Shift, P
        If var = U
            Break
        ImageSearch, x, y, bloodThirstX1, bloodThirstY1, bloodThirstX2, bloodThirstY2, *50 HBITMAP:*%BloodthirstPic%
        if ErrorLevel = 0
            Send, {SHIFTDOWN}%Bloodthirst%{SHIFTUP}
        ImageSearch, x, y, whirlwindX1, whirlwindY1, whirlwindX2, whirlwindY2, *50 HBITMAP:*%WhirlwindPic%
        if ErrorLevel = 0
            Send, {SHIFTDOWN}%Whirlwind%{SHIFTUP}
        ImageSearch, x, y, slamX1, slamY1, slamX2, slamY2, *50 HBITMAP:*%SlamPic%
        if ErrorLevel = 0
            Send, {SHIFTDOWN}%Slam%{SHIFTUP}
        ImageSearch, x, y, heroicThrowX1, heroicThrowY1, heroicThrowX2, heroicThrowY2, *50 HBITMAP:*%HeroicThrowPic%
        if ErrorLevel = 0
            Send, %Heroic%
        ImageSearch, x, y, sunderX1, sunderY1, sunderX2, sunderY2, *50 HBITMAP:*%SunderPic%
        if ErrorLevel = 0
            Send, {SHIFTDOWN}%Sunder%{SHIFTUP}
        ImageSearch, x, y, executeX1, executeY1, executeX2, executeY2, *50 HBITMAP:*%ExecutePic%
        if ErrorLevel = 0
            Send, %Execute%
        Sleep, 100
        Send, {SHIFTDOWN}%Bloodthirst%{SHIFTUP}
    }


    ImageSearch, x, y, bloodThirstX1, bloodThirstY1, bloodThirstX2, bloodThirstY2, *50 HBITMAP:*%BloodthirstPic%
    if ErrorLevel = 0
        Send, %Bloodthirst%
    ImageSearch, x, y, whirlwindX1, whirlwindY1, whirlwindX2, whirlwindY2, *50 HBITMAP:*%WhirlwindPic%
    if ErrorLevel = 0
        Send, %Whirlwind%
    ImageSearch, x, y, slamX1, slamY1, slamX2, slamY2, *50 HBITMAP:*%SlamPic%
    if ErrorLevel = 0
        Send, %Slam%
    ImageSearch, x, y, heroicThrowX1, heroicThrowY1, heroicThrowX2, heroicThrowY2, *50 HBITMAP:*%HeroicThrowPic%
    if ErrorLevel = 0
        Send, %Heroic%
    ImageSearch, x, y, sunderX1, sunderY1, sunderX2, sunderY2, *50 HBITMAP:*%SunderPic%
    if ErrorLevel = 0
        Send, %Sunder%
    ImageSearch, x, y, executeX1, executeY1, executeX2, executeY2, *50 HBITMAP:*%ExecutePic%
    if ErrorLevel = 0
        Send, %Execute%
    Sleep, 100
    Send, %Bloodthirst%
}

#Include, .\setting\pause.ahk
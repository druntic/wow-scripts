F1::

Loop, {
    Send, {CTRLDOWN}t{CTRLUP}
    Sleep, 100
    MouseClick, left,  203,  389
    Sleep, 100
    MouseClick, left,  191,  430
    Sleep, 100
    Send, {Esc}

}

issleeping = 0 ; keep track of status (seems to be no internal variable for this)
; simple loop showing counter in tooltip
; to allow f11 being called twice
#maxthreadsperhotkey 2
F2::
  if !issleeping{
      SoundPlay, Nonexistent.avi
      Soundplay, off.mp3
  }
    
; if resuming from sleep our status flag is set and a second beep is issued
  if issleeping{
      SoundPlay, Nonexistent.avi
      Soundplay, on.mp3
  }
; toogle the status flag
  issleeping := !issleeping
  pause
  return